// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later
import type { comment } from '@/types/shared';
export type title = {
    post: string, 
    headline: string
}

export type position = {
    left?: string, 
    top?: string
}

export type size = {
    height: number,
    width: number
}

export type element = {
    id: string, 
    title: title,
    info?: string,
    styleObject: position, 
    commentLength: number,
    comments: comment[],
    shape: string, 
    color: string,
    body?: any,
    size?: size,
    visible?: boolean,
    authorized: boolean
}

export type globalStyle = any;

export type line = {
    id:string,
    start: string,
    end:string,
    text?:string,//formerly line_text
    dash?:boolean,
    color?:string
}
