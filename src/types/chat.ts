// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later
export type Chat = {
    name: string,
    uuid: string,
    icon?: string,
    avatar?: string,
    type: string,
    image?: boolean,
    message?: string,
    person?: string,
    max_date: string,
    valid_until?: string | null
};

export type Person = {
    name: string,
    username: string,
    description: string,
    categories: string[],
    uuid: string,
    systemprompt: string,
    system_prompt?: string,
    avatar?: string,
    creator?: string,
    model: string
}

export type ChatConfig = {
    name: string,
    systemprompt: string,
    model?: string,
    temperature?: number,
    top_p?: number
}