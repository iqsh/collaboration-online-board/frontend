// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later

export type paneSize = { 
    max_width: number, 
    max_height: number
}

export type lockState = {
    delete: boolean, 
    move: boolean, 
    all: boolean,
    commentable: boolean,
    editHeader?: boolean
}

export type doc = {
    count: number 
    lineArray: any[], 
    deleteObject: any, 
    dataSet: any[], 
    addElement: any,
    addLine: any, 
    unlocked: lockState, 
    style: any, 
    likeAllowed: boolean, 
    commentsAllowed: boolean,
    headline: string, 
    subheadline: string, 
    size?: paneSize
}
