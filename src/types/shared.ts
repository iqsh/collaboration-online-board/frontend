// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later

export type title = {
    post: string, 
    headline: string
}

export type position = {
    left?: string, 
    top?: string
}

export type size = {
    height: number,
    width: number
}

export type comment = {
    id: string,
    parent_id?: string,
    schedule_id?: string,
    body?: any,
    text: string, //formerly message
    type?: string,
    likes: number, //formerly like
    shape: string,
    color: string,
    info?: string,
    visible?: boolean
}

export type element = {
    schedule_id?: string,
    parent_id?:string,
    id: string, 
    title: title, //formerly message
    likes?: number,
    type?: string,
    body?: any,
    color: string,
    shape: string, 
    commentLength: number,
    comments: comment[],
    info?: string,
    order_id?:string,
    visible?: boolean,
    authorized: boolean,
    styleObject?: position,
    size?: size,
}

export type globalStyle = any;