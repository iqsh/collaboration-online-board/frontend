// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later
import type { comment } from '@/types/shared';

export type title = {
    post: string, 
    headline: string
}

export type position = {
    left?: string, 
    top?: string
}

export type size = {
    height: number,
    width: number
}

export type element = {
    id: string, 
    title: title,
    info?: string,
    commentLength: number,
    comments: comment[],
    shape: string, 
    color: string,
    body?: any,
    size?: size,
    visible?: boolean,
    likes:number,
    authorized?: boolean
}

export type globalStyle = any;