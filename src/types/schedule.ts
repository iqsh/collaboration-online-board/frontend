// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later
import type { comment } from '@/types/shared';

export type title = {
    post: string, 
    headline: string
}

export type post = {
    schedule_id?: string,
    parent_id?:string,
    id: string, 
    title: title, //formerly message
    likes: number,
    type?: string,
    body?: any,
    color: string,
    shape: string, 
    commentLength: number,
    comments: comment[],
    info?: string,
    order_id?:string,
    visible?: boolean,
    authorized: boolean
}

export type column = {
    schedule_id?: string,
    id: string,
    title: string,
    postLength: number, //formerly comments
    color: string,
    shape: string,
    order_id?: string
    posts: post[] //formerly data,
    visible?: boolean
}

export type globalStyle = any;
