// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later

export type folder = { 
    name: string,
    id: string,
    color: string,
    parent: string
    subfolders?: folder[],
    panes?: dashboardItem[]
}

export type dashboardItem = {
    background_color: string,
    background_image: string,
    hash_pane: string,
    headline: string,
    last_update: string,
    mail: string,
    subheadline: string,
    type: string,
    folder: string,
    origin_mail?: string,
}