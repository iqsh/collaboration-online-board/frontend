<!-- 
Copyright (c) 2023 Maurizio Bonani
-->

<!--
based on: https://github.com/mauricius/vue-draggable-resizable/tree/main 
date: 2024-03-20 

Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)

SPDX-License-Identifier: AGPL-3.0-or-later
-->
<script setup lang="ts">
import type { PropType } from 'vue';
import { onBeforeMount, onBeforeUnmount, ref, computed, watch } from 'vue';
import { matchesSelectorToParentElements, addEvent, removeEvent } from '@/components/freeform/helper';
import { computeWidth, computeHeight, restrictToBounds, snapToGrid } from '@/components/freeform/helper';
import { onMounted } from 'vue';

// Event Types
const events = {
    mouse: {
        start: 'mousedown',
        move: 'mousemove',
        stop: 'mouseup'
    },
    touch: {
        start: 'touchstart',
        move: 'touchmove',
        stop: 'touchend'
    }
};

let eventsFor = events.mouse;

// Type Definitions
type ScalePropType = number | [number, number];
type MouseClipPostionType = { mouseX: number, mouseY: number, x: number, y: number, w: number, h: number, left: number, right: number, top: number, bottom: number };
type BoundsType = { minLeft: number | null, maxLeft: number | null, minRight: number | null, maxRight: number | null, minTop: number | null, maxTop: number | null, minBottom: number | null, maxBottom: number | null };
type LimitsType = { minLeft: number | null, maxLeft: number | null, minTop: number | null, maxTop: number | null, minRight: number | null, maxRight: number | null, minBottom: number | null, maxBottom: number | null };
type EmitTypes = {
    (event: 'dragging', left: number, top: number): void;
    (event: 'drag-stop', left: number, top: number): void;
    (event: 'resize-stop', left: number, top: number, width: number, height: number): void;
    (event: 'dnd-stop'): void;
};

// Props
const props = defineProps({
    classNameResizing: {
        type: String,
        default: 'resizing'
    },
    classNameHandle: {
        type: String,
        default: 'handle'
    },
    draggable: {
        type: Boolean,
        default: true
    },
    resizable: {
        type: Boolean,
        default: true
    },
    lockAspectRatio: {
        type: Boolean,
        default: false
    },
    w: {
        type: Number,
        default: 200,
        validator: (val: number) => val > 0
    },
    h: {
        type: Number,
        default: 200,
        validator: (val: number) => val > 0
    },
    minHeight: {
        type: Number,
        default: 0,
        validator: (val: number) => val >= 0
    },
    x: {
        type: Number,
        default: 0
    },
    y: {
        type: Number,
        default: 0
    },
    z: {
        type: Number,
        default: 0,
        validator: (val: number) => val >= 0
    },
    dragHandle: {
        type: String,
        default: null
    },
    grid: {
        type: Array as PropType<number[]>,
        default: () => [1, 1],
        validator: (value: unknown): boolean => {
            return Array.isArray(value) && value.length === 2 && typeof value[0] === 'number' && typeof value[1] === 'number';
        }
    },
    scale: {
        type: [Number, Array] as PropType<ScalePropType>,
        default: 1,
        validator: (val: ScalePropType) => {
            if (typeof val === 'number') {
                return val > 0
            }

            return val.length === 2 && val[0] > 0 && val[1] > 0
        }
    },
    onDragStart: {
        type: Function,
        default: () => true
    },
    onResize: {
        type: Function,
        default: () => true
    }
});

// Emits
const emits = defineEmits<EmitTypes>();

// Refs
const left = ref(props.x);
const top = ref(props.y);
const right = ref(0);
const bottom = ref(0);
const width = ref(0);
const height = ref(0);
const widthTouched = ref(false);
const heightTouched = ref(false);
const aspectFactor = ref(0);
const parentWidth = ref(0);
const parentHeight = ref(0);
const handle = ref(null);
const enabled = ref(true);
const resizing = ref(false);
const pinching = ref(false);
const dragging = ref(false);
const dragEnable = ref(false);
const resizeEnable = ref(false);
const zIndex = ref(props.z);
const el = ref<HTMLElement | null>(null);
const mouseClickPosition = ref<MouseClipPostionType>({ mouseX: 0, mouseY: 0, x: 0, y: 0, w: 0, h: 0, left: 0, right: 0, top: 0, bottom: 0 });
const bounds = ref<BoundsType>({ minLeft: null, maxLeft: null, minRight: null, maxRight: null, minTop: null, maxTop: null, minBottom: null, maxBottom: null });
const initialDistance = ref(0);
const initialWidth = ref(0);
const initialHeight = ref(0);

const initialPinchDistance = ref(0);
const lastScale = ref(1);
const smoothScale = ref(1);

// Computed Properties
const style = computed(() => {
    return {
        left: `${left.value}px`,
        top: `${top.value}px`,
        width: computedWidth.value,
        height: computedHeight.value,
        zIndex: zIndex.value,
    }
});

const showHandles = computed(() => {
    return props.resizable;
});

const computedWidth = computed(() => {
    return width.value + 'px';
});

const computedHeight = computed(() => {
    return height.value + 'px';
});

const minW = computed(() => {
    return 150;
});

const minH = computed(() => {
    return props.minHeight;
});

// Watchers
watch(() => props.z, (val: number) => {
    if (val >= 0) zIndex.value = val;
});

watch(() => props.x, (val: number) => {
    if (resizing.value || dragging.value) {
        return;
    }
    bounds.value = calcDragLimits();
    moveHorizontally(val);
});

watch(() => props.y, (val: number) => {
    if (resizing.value || dragging.value) {
        return;
    }
    bounds.value = calcDragLimits();
    moveVertically(val);
});

watch(() => props.lockAspectRatio, (val: boolean) => {
    if (val) {
        aspectFactor.value = width.value / height.value;
    } else {
        aspectFactor.value = 0
    }
});

watch(() => props.w, (val: number) => {
    if (dragging.value) {
        return;
    }
    bounds.value = calcResizeLimits();
    changeWidth(val);
});

watch(() => props.h, (val: number) => {
    if (dragging.value) {
        return;
    }
    bounds.value = calcResizeLimits();
    changeHeight(val);
});

// Event Handlers
const handleTouchStart = (e: TouchEvent) => {
    if (e.touches.length === 2) {
        handlePinchStart(e);
    } else if (e.touches.length === 1) {
        handleStartPressEventTouch(e);
    }
};

const handlePinchStart = (e: TouchEvent) => {
    e.preventDefault();
    initialDistance.value = getTouchDistance(e.touches);
    initialWidth.value = width.value;
    initialHeight.value = height.value;
    initialPinchDistance.value = getTouchDistance(e.touches);
    lastScale.value = 1;
    smoothScale.value = 1;
    addEvent(document.documentElement, 'touchmove', handleTouchMove);
    addEvent(document.documentElement, 'touchend', handleTouchEnd);
    handleResetLongPressEvent();
};

const getTouchDistance = (touches: TouchList) => {
    const dx = touches[0].clientX - touches[1].clientX;
    const dy = touches[0].clientY - touches[1].clientY;
    return Math.sqrt(dx * dx + dy * dy);
};

const handleTouchMove = (e: TouchEvent) => {
    if (e.touches.length === 2) {
        handlePinchMove(e);
    } else if (e.touches.length === 1) {
        move(e);
    }
};



const handlePinchMove = (e: TouchEvent) => {
    if (e.touches.length !== 2) return;
    if (dragging.value) return;

    e.preventDefault();
    const newDistance = getTouchDistance(e.touches);
    if (newDistance < 65) {
        handleTouchEnd(e);
        return;
    }
    const rawScale = newDistance / initialPinchDistance.value;
    const logScale = Math.log2(rawScale) + 1;
    const MAX_SCALE = 1.5;
    const MIN_SCALE = 0.5;
    const clampedScale = Math.min(Math.max(logScale, MIN_SCALE), MAX_SCALE);
    lastScale.value = clampedScale;

    const SMOOTHING_FACTOR = 0.3;
    smoothScale.value += (clampedScale - smoothScale.value) * SMOOTHING_FACTOR;
    const newWidth = initialWidth.value * smoothScale.value;
    changeWidth(newWidth);

    if (props.onResize(handle.value, left.value, top.value, width.value, height.value) === false) {
        return;
    }
    pinching.value = true;
};

const handleTouchEnd = (e: TouchEvent) => {
    removeEvent(document.documentElement, 'touchmove', handleTouchMove);
    removeEvent(document.documentElement, 'touchend', handleTouchEnd);
    handleResetLongPressEvent();

    if (resizing.value) {
        resizing.value = false;
        emits('resize-stop', left.value, top.value, width.value, height.value);
    }

    if (pinching.value) {
        pinching.value = false;
        emits('resize-stop', left.value, top.value, width.value, height.value);
    }

    if (dragging.value) {
        dragging.value = false;
        emits('drag-stop', left.value, top.value);
    }
};

const elementTouchDown = (e: TouchEvent) => {
    eventsFor = events.touch;
    elementDown(e);
};

const elementMouseDown = (e: MouseEvent) => {
    eventsFor = events.mouse;
    elementDown(e);
};

const elementDown = (e: TouchEvent | MouseEvent) => {
    if (e instanceof MouseEvent && e.button !== 0) {
        return;
    }

    const target = e.target as Node | null;

    if (el.value && target && el.value.contains(target)) {
        if (props.onDragStart(e) === false) {
            return;
        }

        if ((props.dragHandle && !matchesSelectorToParentElements(target, props.dragHandle, el.value)) ||
            (null && matchesSelectorToParentElements(target, null, el.value))) {
            dragging.value = false;
            emits('drag-stop', left.value, top.value);
            return;
        }

        if (props.draggable) {
            dragEnable.value = true;
        }

        mouseClickPosition.value.mouseX = 'touches' in e ? e.touches[0].pageX : e.pageX;
        mouseClickPosition.value.mouseY = 'touches' in e ? e.touches[0].pageY : e.pageY;

        mouseClickPosition.value.left = left.value;
        mouseClickPosition.value.right = right.value;
        mouseClickPosition.value.top = top.value;
        mouseClickPosition.value.bottom = bottom.value;

        bounds.value = calcDragLimits();

        addEvent(document.documentElement, eventsFor.move, move);
        addEvent(document.documentElement, eventsFor.stop, handleUp);

    }
};

const calcDragLimits = () => {
    return {
        minLeft: left.value % props.grid[0],
        maxLeft: Math.floor((parentWidth.value - width.value - left.value) / props.grid[0]) * props.grid[0] + left.value,
        minRight: right.value % props.grid[0],
        maxRight: Math.floor((parentWidth.value - width.value - right.value) / props.grid[0]) * props.grid[0] + right.value,
        minTop: top.value % props.grid[1],
        maxTop: Math.floor((parentHeight.value - height.value - top.value) / props.grid[1]) * props.grid[1] + top.value,
        minBottom: bottom.value % props.grid[1],
        maxBottom: Math.floor((parentHeight.value - height.value - bottom.value) / props.grid[1]) * props.grid[1] + bottom.value
    };
};

const deselect = (e: MouseEvent | TouchEvent) => {
    const target = e.target as Node | null;

    if (!(el.value && target && el.value.contains(target))) {
        removeEvent(document.documentElement, eventsFor.move, handleResize);
    }

    resetBoundsAndMouseState();
};

const handleTouchDown = (handle: any, e: TouchEvent) => {
    eventsFor = events.touch;
    handleDown(handle, e);
};

const handleDown = (localHandle: any, e: TouchEvent | MouseEvent) => {
    if (e instanceof MouseEvent && e.button !== 0) {
        return;
    }
    if (e instanceof TouchEvent && e.touches.length > 1) {
        return;
    }

    if (e.stopPropagation) e.stopPropagation();

    handle.value = localHandle;

    resizeEnable.value = true;

    mouseClickPosition.value.mouseX = 'touches' in e ? e.touches[0].pageX : e.pageX;
    mouseClickPosition.value.mouseY = 'touches' in e ? e.touches[0].pageY : e.pageY;
    mouseClickPosition.value.left = left.value;
    mouseClickPosition.value.right = right.value;
    mouseClickPosition.value.top = top.value;
    mouseClickPosition.value.bottom = bottom.value;

    bounds.value = calcResizeLimits();

    addEvent(document.documentElement, eventsFor.move, handleResize);
    addEvent(document.documentElement, eventsFor.stop, handleUp);
};

const calcResizeLimits = () => {
    let localMinW = minW.value;
    let localMinH = minH.value;

    const [gridX, gridY] = props.grid;

    if (props.lockAspectRatio) {
        if (localMinW / localMinH > aspectFactor.value) {
            localMinH = localMinW / aspectFactor.value;
        } else {
            localMinW = aspectFactor.value * localMinH;
        }
    }

    const limits: LimitsType = {
        minLeft: null,
        maxLeft: null,
        minTop: null,
        maxTop: null,
        minRight: null,
        maxRight: null,
        minBottom: null,
        maxBottom: null
    };

    limits.minLeft = left.value % gridX;
    limits.maxLeft = left.value + Math.floor((width.value - localMinW) / gridX) * gridX;
    limits.minTop = top.value % gridY;
    limits.maxTop = top.value + Math.floor((height.value - localMinH) / gridY) * gridY;
    limits.minRight = right.value % gridX;
    limits.maxRight = right.value + Math.floor((width.value - localMinW) / gridX) * gridX;
    limits.minBottom = bottom.value % gridY;
    limits.maxBottom = bottom.value + Math.floor((height.value - localMinH) / gridY) * gridY;

    if (props.lockAspectRatio) {
        limits.minLeft = Math.max(limits.minLeft, left.value - top.value * aspectFactor.value);
        limits.minTop = Math.max(limits.minTop, top.value - left.value / aspectFactor.value);
        limits.minRight = Math.max(limits.minRight, right.value - bottom.value * aspectFactor.value);
        limits.minBottom = Math.max(limits.minBottom, bottom.value - right.value / aspectFactor.value);
    }

    return limits;
};

const move = (e: TouchEvent | MouseEvent) => {
    if (resizing.value) {
        handleResize(e);
    } else if (dragEnable.value) {
        handleDrag(e);
    }
};

const handleDrag = (e: TouchEvent | MouseEvent) => {
    if (e instanceof TouchEvent && e.touches.length > 1) {
        return;
    }
    const grid = props.grid;

    const tmpDeltaX = mouseClickPosition.value.mouseX - ('touches' in e ? e.touches[0].pageX : e.pageX);
    const tmpDeltaY = mouseClickPosition.value.mouseY - ('touches' in e ? e.touches[0].pageY : e.pageY);

    const [deltaX, deltaY] = snapToGrid(grid, tmpDeltaX, tmpDeltaY, props.scale);
    left.value = restrictToBounds(mouseClickPosition.value.left - deltaX, bounds.value.minLeft, bounds.value.maxLeft);
    top.value = restrictToBounds(mouseClickPosition.value.top - deltaY, bounds.value.minTop, bounds.value.maxTop);

    right.value = restrictToBounds(mouseClickPosition.value.right + deltaX, bounds.value.minRight, bounds.value.maxRight);
    bottom.value = restrictToBounds(mouseClickPosition.value.bottom + deltaY, bounds.value.minBottom, bounds.value.maxBottom);

    emits('dragging', left.value, top.value);
    dragging.value = true;
};

const moveHorizontally = (val: any) => {
    const [deltaX, _] = snapToGrid(props.grid, val, top.value, 1);
    left.value = restrictToBounds(deltaX, bounds.value.minLeft, bounds.value.maxLeft);
    right.value = parentWidth.value - width.value - left.value;
};

const moveVertically = (val: any) => {
    const [_, deltaY] = snapToGrid(props.grid, left.value, val, 1);
    top.value = restrictToBounds(deltaY, bounds.value.minTop, bounds.value.maxTop);
    bottom.value = parentHeight.value - height.value - top.value;
};

const handleResize = (e: MouseEvent | TouchEvent) => {
    if (e instanceof TouchEvent && e.touches.length > 1) {
        handleTouchEnd(e);
        return;
    }
    let localRight = right.value;
    let localBottom = bottom.value;

    const tmpDeltaX = mouseClickPosition.value.mouseX - ('touches' in e ? e.touches[0].pageX : e.pageX);
    const tmpDeltaY = mouseClickPosition.value.mouseY - ('touches' in e ? e.touches[0].pageY : e.pageY);

    if (!widthTouched.value && tmpDeltaX) {
        widthTouched.value = true;
    }

    if (!heightTouched.value && tmpDeltaY) {
        heightTouched.value = true;
    }

    const [deltaX, deltaY] = snapToGrid(props.grid, tmpDeltaX, tmpDeltaY, props.scale);
    localRight = restrictToBounds(
        mouseClickPosition.value.right + deltaX,
        bounds.value.minRight,
        bounds.value.maxRight
    );

    if (props.lockAspectRatio) {
        localBottom = bottom.value - (right.value - localRight) / aspectFactor.value;
    }
    const localWidth = computeWidth(parentWidth.value, left.value, localRight);


    width.value = localWidth > (width.value + 10) ? (width.value + 10) : localWidth;
    height.value = computeHeight(parentHeight.value, top.value, localBottom);

    if (props.onResize(handle.value, left.value, top.value, width.value, height.value) === false) {
        return;
    }

    bottom.value = localBottom;
    right.value = localRight;
    resizing.value = true;
};

const changeWidth = (val: any) => {
    const [newWidth, _] = snapToGrid(props.grid, val, 0, 1);
    const localRight = restrictToBounds(
        (parentWidth.value - newWidth - left.value),
        bounds.value.minRight,
        bounds.value.maxRight
    );
    let localBottom = bottom.value;
    if (props.lockAspectRatio) {
        localBottom = bottom.value - (right.value - localRight) / aspectFactor.value;
    }
    const localWidth = computeWidth(parentWidth.value, left.value, localRight);
    //width.value = localWidth > (width.value + 10) ? (width.value) : localWidth;
    width.value = localWidth > val ? val : localWidth;
    height.value = computeHeight(parentHeight.value, top.value, localBottom);
    right.value = localRight;
    bottom.value = localBottom;
};

const changeHeight = (val: any) => {
    const [_, newHeight] = snapToGrid(props.grid, 0, val, 1);

    let localBottom = restrictToBounds(
        (parentHeight.value - newHeight - top.value),
        bounds.value.minBottom,
        bounds.value.maxBottom
    );

    if (props.lockAspectRatio) {
        right.value = right.value - (bottom.value - localBottom) * aspectFactor.value;
    }

    const localWidth = computeWidth(parentWidth.value, left.value, right.value);
    width.value = localWidth > (width.value + 10) ? (width.value) : localWidth;
    const localHeight = computeHeight(parentHeight.value, top.value, localBottom);
    if (localHeight > val) {
        height.value = val;
        bounds.value = calcResizeLimits();
        localBottom = restrictToBounds(
            (parentHeight.value - newHeight - top.value),
            bounds.value.minBottom,
            bounds.value.maxBottom
        );
    } else {
        height.value = localHeight;
    }
    //height.value = localHeight > val? val:localHeight;

    bottom.value = localBottom;
};

const handleUp = (e: any) => {
    handle.value = null;

    resetBoundsAndMouseState();

    dragEnable.value = false;
    resizeEnable.value = false;

    if (resizing.value) {
        resizing.value = false;
        emits('resize-stop', left.value, top.value, width.value, height.value);
    }

    if (dragging.value) {
        dragging.value = false;
        emits('drag-stop', left.value, top.value);
    }

    emits('dnd-stop');

    removeEvent(document.documentElement, eventsFor.move, handleResize);
};

// Helper Functions
const resetBoundsAndMouseState = () => {
    mouseClickPosition.value = { mouseX: 0, mouseY: 0, x: 0, y: 0, w: 0, h: 0, left: 0, right: 0, top: 0, bottom: 0 };

    bounds.value = {
        minLeft: null,
        maxLeft: null,
        minRight: null,
        maxRight: null,
        minTop: null,
        maxTop: null,
        minBottom: null,
        maxBottom: null
    };
};

const checkParentSize = () => {
    const [newParentWidth, newParentHeight] = getParentSize();
    parentWidth.value = newParentWidth;
    parentHeight.value = newParentHeight;
    right.value = parentWidth.value - width.value - left.value;
    bottom.value = parentHeight.value - height.value - top.value;
};

const getParentSize = () => {
    if (el.value && el.value.parentNode && el.value.parentNode instanceof Element) {
        const style = window.getComputedStyle(el.value.parentNode, null);

        return [
            parseInt(style.getPropertyValue('width'), 10),
            parseInt(style.getPropertyValue('height'), 10)
        ];
    }
    return [8000, 8000];
};

// Lifecycle Hooks
onBeforeMount(() => {
    resetBoundsAndMouseState();
});

onMounted(() => {
    if (el.value) {
        el.value.ondragstart = () => false;

        const [pw, ph] = getParentSize();

        parentWidth.value = pw;
        parentHeight.value = ph;

        aspectFactor.value = props.w / props.h;

        width.value = props.w;
        height.value = props.h;

        right.value = parentWidth.value - width.value - left.value;
        bottom.value = parentHeight.value - height.value - top.value;

        addEvent(document.documentElement, 'mousedown', deselect);
        addEvent(document.documentElement, 'touchend touchcancel', deselect);
        addEvent(window, 'resize', checkParentSize);
    }
});

onBeforeUnmount(() => {
    removeEvent(document.documentElement, 'mousedown', deselect);
    removeEvent(document.documentElement, 'touchstart', handleUp);
    removeEvent(document.documentElement, 'mousemove', move);
    removeEvent(document.documentElement, 'touchmove', handleTouchMove);
    removeEvent(document.documentElement, 'mouseup', handleUp);
    removeEvent(document.documentElement, 'touchend touchcancel', deselect);

    removeEvent(window, 'resize', checkParentSize);
});

// Long Press Handlers
let pressTimer: any;

const handleStartPressEventTouch = (e: any) => {
    pressTimer = setTimeout(() => {
        dragging.value = true;
        elementTouchDown(e);
    }, 500);
};

const handleResetLongPressEvent = () => {
    clearTimeout(pressTimer);
};

</script>
<template>
    <div ref="el" :style="style" class="select-none" :class="[{
        'active': enabled,
        'dragging': dragging,
        [classNameResizing]: resizing || pinching,
        'draggable': draggable,
        'resizable': resizable
    }, 'absolute']" @mousedown="elementMouseDown" @mouseup="" @touchstart="handleTouchStart"
        @touchend="handleResetLongPressEvent">
        <div v-if="showHandles" key="handle_mr" :class="[enabled ? 'block' : 'hidden']"
            class="w-2 absolute h-full -right-1 cursor-e-resize bg-transparent border-0"
            @mousedown.stop.prevent="handleDown('mr', $event)" @touchstart.stop.prevent="handleTouchDown('mr', $event)">
            <slot name="mr"></slot>
        </div>
        <slot></slot>
    </div>
</template>