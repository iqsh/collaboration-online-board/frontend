type MatchesFunctionName = 'matches' | 'webkitMatchesSelector' | 'mozMatchesSelector' | 'msMatchesSelector' | 'oMatchesSelector';

export function matchesSelectorToParentElements(el: Node, selector: string | null, baseNode: Element): boolean {
    let node: Node | null = el;

    const matchesSelectorFunc = [
        'matches',
        'webkitMatchesSelector',
        'mozMatchesSelector',
        'msMatchesSelector',
        'oMatchesSelector'
    ].find(func => isFunction((node as any)[func])) as MatchesFunctionName | undefined;

    if (matchesSelectorFunc && !isFunction((node as any)[matchesSelectorFunc])) return false;

    do {
        // @ts-ignore
        if (matchesSelectorFunc && node[matchesSelectorFunc](selector)) return true;
        if (node === baseNode) return false;
        node = node.parentNode as Element | null;
    } while (node);

    return false;
};

export function addEvent(el: EventTarget | null, event: string, handler: any): void {
    if (!el) {
        return;
    }
    el.addEventListener(event, handler, true);
};

export function removeEvent(el: EventTarget | null, event: string, handler: any): void {
    if (!el) {
        return;
    }
    el.removeEventListener(event, handler, true);
};

export function isFunction(func: any): boolean {
    return (typeof func === 'function' || Object.prototype.toString.call(func) === '[object Function]');
};

export function snapToGrid(grid: number[], pendingX: number, pendingY: number, scale: number | [number, number] = 1): [number, number] {
    const [scaleX, scaleY] = typeof scale === 'number' ? [scale, scale] : scale;
    const x = Math.round((pendingX / scaleX) / grid[0]) * grid[0];
    const y = Math.round((pendingY / scaleY) / grid[1]) * grid[1];
    return [x, y];
};

export function getSize(el: Element): [number, number] {
    const rect = el.getBoundingClientRect();

    return [
        parseInt(rect.width.toString()),
        parseInt(rect.height.toString())
    ];
};

export function computeWidth(parentWidth: number, left: number, right: number): number {
    return parentWidth - left - right;
};

export function computeHeight(parentHeight: number, top: number, bottom: number): number {
    return parentHeight - top - bottom;
};

export function restrictToBounds(value: number, min: number | null, max: number | null): number {
    if (min !== null && value < min) {
        return min;
    }

    if (max !== null && max < value) {
        return max;
    }

    return value;
};