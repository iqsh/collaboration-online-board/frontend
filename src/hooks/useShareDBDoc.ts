// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import { inject, ref, onMounted } from 'vue';
import { useRouter } from 'vue-router';
import { storeToRefs } from 'pinia';
import { useI18n } from 'vue-i18n';
import { connectionInitialize } from '@/lib/connection';
import { notificationStatus, paneTypes } from '@/enums/enums';
import { useFreeformStore } from '@/stores/freeform';
import { useMetaStore } from '@/stores/meta';
import { useBaseStore } from '@/stores/base';

export default function useShareDBDoc(type: paneTypes) {
    const baseStore = useBaseStore();
    const { message, status } = storeToRefs(baseStore);
    const metaStore = useMetaStore();
    const freeformStore = useFreeformStore();
    const { lineArray } = storeToRefs(freeformStore);
    const { hashcode, doc, dataSet } = storeToRefs(metaStore);
    const connection = ref();
    const router = useRouter();
    const configuration = inject<any>('configuration');

    const { t } = useI18n();
    const $t = t;

    onMounted(() => {
        if (hashcode.value && configuration.wsServer) {
            connection.value = connectionInitialize(configuration.wsServer + hashcode.value, { message: $t("general.errors.connectionLost"), ok: $t("general.buttons.ok") });
        } else {
            message.value = $t('indexPage.error.notFound');
            status.value = notificationStatus.DANGER;
            router.push({ name: "home" });
            return;
        }
        const connectionDocument = connection.value.get('boards', hashcode.value);
        connectionDocument.on('error', (err: any) => console.error(err));

        const changeValues = () => {
            setTimeout(() => {
                doc.value = null;
                doc.value = connectionDocument;
                dataSet.value = null;
                dataSet.value = connectionDocument.data ? connectionDocument.data.dataSet : [];
                if (type === paneTypes.FREEFORM) {
                    lineArray.value = null;
                    lineArray.value = connectionDocument.data ? connectionDocument.data.lineArray : [];
            }
            }, 100);
        }

        connectionDocument.subscribe(changeValues);
        connectionDocument.on('op batch', changeValues);
    });
}