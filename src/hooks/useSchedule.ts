// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import { storeToRefs } from 'pinia';
import { inject } from 'vue';
import { request } from '@/lib/request';
import { checkForURL, textClear, uploadFile } from '@/lib/utils';
import { useScheduleStore } from '@/stores/schedule';
import { useI18n } from 'vue-i18n';
import { uploadPaneTypes, handleModeEnum, userType, notificationStatus } from '@/enums/enums';
import { useBaseStore } from '@/stores/base';


export default function useSchedule() {
    const { t } = useI18n();
    const $t = t;
    const store = useScheduleStore();
    const baseStore = useBaseStore();
    const { file, handleMode, getFullUsable, getEmbeddedsAllowed, getStyle, getUserType, getHashcode, showLoadingSpinner, postIndex, columnIndex, dataSet: getDataSet, commentIndex, elementColor } = storeToRefs(store);
    const { message, status } = storeToRefs(baseStore);
    let configuration = inject<any>('configuration');

    const createPost = async ({ title, text, info }: { title?: string, text?: string, info?: string }) => {
        if (getFullUsable) {
            let result = null;
            showLoadingSpinner.value = true;
            let isEmbedded = false;
            if (file.value !== null) {
                try {
                    result = await uploadFile(file.value, getHashcode.value, uploadPaneTypes.SCHEDULE, configuration.apiServer);
                }
                catch (error) {
                    showLoadingSpinner.value = false;
                    console.error(error);
                    message.value = $t('contentPage.media.uploadError');
                    status.value = notificationStatus.DANGER;
                    return;
                }
            } else if (text !== null && text !== undefined && getEmbeddedsAllowed && checkForURL(text)) {
                isEmbedded = true;
                try {
                    const response = await request('GET', configuration.apiServer + '/api/unfurl/' + encodeURIComponent(textClear(text).replace(/<p>/g, '').replace(/<\/p>/g, '')));
                    if (response !== null && response.status !== 404) {
                        result = { body: response };
                    }
                } catch (error) {
                    console.error(error);
                }
            }
            if (title || (result && result.path) || text) {
                const localText = result?.path ? result.path.replace('&amp;', '&') : (text ? (isEmbedded ? textClear(text).replace(/<p>/g, '').replace(/<\/p>/g, '').replace('&amp;', '&') : text) : "");
                store.createPost({ columnIndex: columnIndex.value, title: title, text: localText, info: info, body: result ? result.body : undefined, color: elementColor.value });
            }
            if (getUserType.value === userType.USER && getStyle.value.authorize)
                message.value = $t('contentPage.post.notification.authorize.label');
            showLoadingSpinner.value = false;
            file.value = null;
            handleMode.value = handleModeEnum.NONE;
            elementColor.value = getStyle.value.commentColor;
            commentIndex.value = -1;
        }
    }

    const editPost = async ({ title, text, info, deleteFile }: { title?: string, text?: string, info?: string, deleteFile?: boolean }) => {
        if (getFullUsable) {
            let result = null;
            showLoadingSpinner.value = true;
            let success = true;
            let isEmbedded = false;
            if (deleteFile) {
                success = false;
                const path = commentIndex.value !== -1 ? getDataSet.value![columnIndex.value].posts[postIndex.value].comments[commentIndex.value].post : getDataSet.value![columnIndex.value].posts[postIndex.value].title.post;
                const options = { hashcode: getHashcode.value, t: 's', path: path };
                let response = await request('DELETE', configuration.apiServer + "/api/file/delete/", options);
                success = response.success;
            }
            if (file.value !== null) {
                try {
                    result = await uploadFile(file.value, getHashcode.value, uploadPaneTypes.SCHEDULE, configuration.apiServer);
                }
                catch (error) {
                    showLoadingSpinner.value = false;
                    console.error(error);
                    message.value = $t('contentPage.media.uploadError');
                    status.value = notificationStatus.DANGER;
                    return;
                }
            } else if (text !== null && text !== undefined && getEmbeddedsAllowed && checkForURL(text)) {
                isEmbedded = true;
                try {
                    const response = await request('GET', configuration.apiServer + '/api/unfurl/' + encodeURIComponent(textClear(text).replace(/<p>/g, '').replace(/<\/p>/g, '')));
                    if (response !== null && response.status !== 404) {
                        result = { body: response };
                    }
                } catch (error) {
                    console.error(error);
                }
            }
            if (success) {
                if (title || (result && result.path) || text) {
                    const localText = result?.path ? result.path.replace('&amp;', '&') : (text ? (isEmbedded ? textClear(text).replace(/<p>/g, '').replace(/<\/p>/g, '').replace('&amp;', '&') : text) : "");
                    const object = {
                        postIndex: postIndex.value, columnIndex: columnIndex.value, text: localText, color: elementColor.value,
                        info: info, body: result && result.body ? result.body : (isEmbedded || localText.includes('/api/file') ? undefined : null)
                    };
                    if (commentIndex.value !== -1) {
                        await store.editCommentProps({ ...object, commentIndex: commentIndex.value });
                    } else
                        store.editPostProps({ ...object, title });
                }
                commentIndex.value = -1;
                showLoadingSpinner.value = false;
                file.value = null;
                elementColor.value = getStyle.value.commentColor;
            }
        }
    }

    const deletePost = async ({ postIndex, columnIndex }: { postIndex: number, columnIndex: number }) => {
        const options = { hashcode: getHashcode.value, t: 's', data: [JSON.parse(JSON.stringify(getDataSet.value![columnIndex].posts[postIndex]))] };
        let response = await request('DELETE', configuration.apiServer + "/api/file/delete/all", options);
        if (response.success) {
            store.deletePost({ postIndex, columnIndex });
        }
    }

    const deleteColumn = async ({ columnIndex }: { columnIndex: number }) => {
        const posts = JSON.parse(JSON.stringify(getDataSet.value![columnIndex].posts));;
        const options = { hashcode: getHashcode.value, t: 's', data: posts };
        let response = await request('DELETE', configuration.apiServer + "/api/file/delete/all", options)
        if (response && response.success)
            store.deleteColumn({ columnIndex: columnIndex });
    }

    /*const updateLinePositions = ()=>{
        lineObjects.value.forEach((element:any) => {
            element.position();
        });
    }*/

    return { createPost, editPost, deletePost, deleteColumn };
}