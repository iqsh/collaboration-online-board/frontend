// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import { storeToRefs } from 'pinia';
import { inject } from 'vue';
import { request } from '@/lib/request';
import { uploadFile, textClear, checkForURL } from '@/lib/utils';
import { useFreeformStore } from '@/stores/freeform';
import { useI18n } from 'vue-i18n';
import { uploadPaneTypes, handleModeEnum, notificationStatus, userType } from '@/enums/enums';
import type { element } from "@/types/freeform";
import { useBaseStore } from '@/stores/base';


export default function useFreeform() {
    const { t } = useI18n();
    const $t = t;
    const store = useFreeformStore();
    const baseStore = useBaseStore();
    const { file, elementId, handleMode, getFullUsable, getEmbeddedsAllowed, dataSet: getDataSet, getStyle, getHashcode, showLoadingSpinner, getUserType, commentIndex, elementColor, postIndex } = storeToRefs(store);
    const { message, status } = storeToRefs(baseStore);

    let configuration = inject<any>('configuration');

    const createPost = async ({ title, text, info, top, left }: { title?: string, text?: string, info?: string, top?: number, left?: number }) => {
        if (getFullUsable.value) {
            const postTop = top ? top : 60 + document.documentElement.scrollTop;
            const postLeft = left ? left : 10 + document.documentElement.scrollLeft;
            let result = null;
            showLoadingSpinner.value = true;
            let isEmbedded = false;
            if (file.value !== null) {
                try {
                    result = await uploadFile(file.value, getHashcode.value, uploadPaneTypes.FREEFORM, configuration.apiServer);
                }
                catch (error) {
                    showLoadingSpinner.value = false;
                    console.error(error);
                    message.value = $t('contentPage.media.uploadError');
                    status.value = notificationStatus.DANGER;
                    return;
                }
            } else if (text !== null && text !== undefined && getEmbeddedsAllowed && checkForURL(text)) {
                isEmbedded = true;
                try {
                    const response = await request('GET', configuration.apiServer + '/api/unfurl/' + encodeURIComponent(textClear(text).replace(/<p>/g, '').replace(/<\/p>/g, '')));
                    if (response !== null && response.status !== 404) {
                        result = { body: response };
                    }
                } catch (error) {
                    console.error(error);
                }
            }
            if (title || (result && result.path) || text) {
                const localText = result?.path ? result.path.replace('&amp;', '&') : (text ? (isEmbedded ? textClear(text).replace(/<p>/g, '').replace(/<\/p>/g, '').replace('&amp;', '&') : text) : "");
                elementId.value = store.createPost({
                    color: elementColor.value, shape: getStyle.value.shape,
                    title: title, text: localText, info: info, body: result ? result.body : undefined,
                    left: postLeft, top: postTop
                });
            }
            if (getUserType.value === userType.USER && getStyle.value.authorize)
                message.value = $t('contentPage.post.notification.authorize.label');
            showLoadingSpinner.value = false;
            file.value = null;
            handleMode.value = handleModeEnum.NONE;
            elementColor.value = getStyle.value.shapeColor;
            commentIndex.value = -1;
        }
    }

    const editPost = async ({ title, text, info, deleteFile }: { title?: string, text?: string, info?: string, deleteFile?: boolean }) => {
        if (getFullUsable.value) {
            let result = null;
            showLoadingSpinner.value = true;
            let success = true;
            let isEmbedded = false;
            if (deleteFile) {
                success = false;
                const path = commentIndex.value !== -1 ? getDataSet.value!.find((x: element) => x.id === elementId.value).comments[commentIndex.value].post : getDataSet.value!.find((x: element) => x.id === elementId.value).title.post
                const options = { hashcode: getHashcode.value, t: 'f', path: path };
                let response = await request('DELETE', configuration.apiServer + "/api/file/delete/", options);
                success = response.success;
            }
            if (file.value !== null) {
                try {
                    result = await uploadFile(file.value, getHashcode.value, uploadPaneTypes.FREEFORM, configuration.apiServer);
                }
                catch (error) {
                    showLoadingSpinner.value = false;
                    console.error(error);
                    message.value = $t('contentPage.media.uploadError');
                    status.value = notificationStatus.DANGER;
                    return;
                }
            } else if (text !== null && text !== undefined && getEmbeddedsAllowed && checkForURL(text)) {
                isEmbedded = true;
                try {
                    const response = await request('GET', configuration.apiServer + '/api/unfurl/' + encodeURIComponent(textClear(text).replace(/<p>/g, '').replace(/<\/p>/g, '')));

                    if (response !== null && response.status !== 404) {
                        result = { body: response };
                    }
                } catch (error) {
                    console.error(error);
                }
            }
            if (success) {
                if (title || (result && result.path) || text) {
                    const localText = result?.path ? result.path.replace('&amp;', '&') : (text ? (isEmbedded ? textClear(text).replace(/<p>/g, '').replace(/<\/p>/g, '').replace('&amp;', '&') : text) : "");
                    const object = {
                        postId: elementId.value, title: title, text: localText, color: elementColor.value,
                        info: info, body: result && result.body ? result.body : (isEmbedded || localText.includes('/api/file') ? undefined : null)
                    };
                    if (commentIndex.value !== -1) {
                        store.editCommentProps({ ...object, commentIndex: commentIndex.value, postIndex: postIndex.value });
                        commentIndex.value = -1;
                    }
                    else
                        store.editPostProps(object);
                }
                showLoadingSpinner.value = false;
                file.value = null;
                elementColor.value = getStyle.value.shapeColor;
            }
        }
    }

    const deletePost = async ({ postIndex }: { postIndex: number }) => {
        const options = { hashcode: getHashcode.value, t: 'f', data: [JSON.parse(JSON.stringify(getDataSet.value![postIndex]))] };
        let response = await request('DELETE', configuration.apiServer + "/api/file/delete/all", options);
        if (response.success) {
            store.deletePost({ postIndex: postIndex });
        }
    }

    /*const updateLinePositions = ()=>{
        lineObjects.value.forEach((element:any) => {
            element.position();
        });
    }*/

    return { createPost, editPost, deletePost };
}