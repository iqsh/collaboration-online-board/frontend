// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later

export enum itemEnum {
    NAME = "name", 
    VALUE = "value", 
    ID = "id"
}

export enum paneTypes {
    FREEFORM = "freeform",
    SCHEDULE = "schedule",
    STREAM = "stream",
    TIMELINE = "timeline",
    NONE = "none",
    ALL = "all"
}

export enum userType {
    ADMIN = "admin",
    MODERATOR = "moderator",
    USER = "user"
}

export enum uploadPaneTypes {
    FREEFORM = "f",
    SCHEDULE = "s",
    TIMELINE = "t"
}

export enum handleModeEnum {
    NONE = "",
    EDIT = "edit",
    ADD =  "add",
    DELETE = "delete",
    COMMENT_EDIT = "commentEdit",
    COMMENT_ADD = "commentAdd",
    COMMENT_DELETE = "commentDelete"
}

export enum sortEnum{
    DESC = "descanding",
    ASC = "ascending"
}

export enum notificationStatus{
    SUCCESS = "success",
    DANGER = "danger"
}

export enum adminMenuEnum{
    NONE = "",
    QRCODE = "qrcode",
    SHARE = "share",
    COPY = "copy",
    DELETE = "delete", 
    EXPORTPDF = "exportpdf",
    PASSWORDRESET = "passwordreset",
    SETTINGS = "settings"
   
}