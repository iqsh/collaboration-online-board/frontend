// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import { createRouter, createWebHistory } from 'vue-router';
import { inject } from 'vue';
import { generateUUID, getCookie } from '@/lib/utils';
//@ts-ignore
import IndexView from '../views/IndexView.vue';
import { request } from '@/lib/request';
import { i18n } from '@/main';
import { useBaseStore } from '@/stores/base';
import { notificationStatus } from '@/enums/enums';

async function checkIfAuthenticated(to: any, from: any) {
  if (to.name !== 'login' && getCookie('Token') === undefined) {
    return { name: 'login' };
  }
};

async function goToDashboard(to: any, from: any) {
  if (to.name === 'impress' || to.name === 'privacy' || to.name === 'password-reset') {
    return true;
  } 
  else if(to.name === 'registration' || to.name === 'registration-account'){
    if(getCookie('token')!==undefined){
      return { name: 'overview-account' };
    }else{
      return true;
    }
  }else{
    if (getCookie('Token') !== undefined) {
      return { name: 'overview-account' };
    }
  }
};

async function checkIfAuthorizedForAi(to: any, from: any) {
  const token = getCookie('Token');
  if (token) {
    const configuration = inject('configuration') as { apiServer: string };
    const result = (await request('GET', `${configuration.apiServer}/api/ai/authorized`, undefined, undefined, false, true)).success;
    if (result) {
      return true;
    } else {
      return { name: 'overview-account' };
    }
  }
  return false;
}

async function checkIfExisting(to: any, from: any) {
  const configuration = inject('configuration') as { apiServer: string };
  const result = (await request('GET', `${configuration.apiServer}/api/pane/${to.params.id}/moderation`)).success;
  if (result)
    return true;
  else {
    const { t } = i18n.global;
    const baseStore = useBaseStore();
    if (getCookie('Token') !== undefined) {
      setTimeout(() => {
        baseStore.$patch({ message: t('general.errors.noPasswordSet'), status: notificationStatus.DANGER });
      }, 500);
      return { name: 'overview-account' };
    } else {
      setTimeout(() => {
        baseStore.$patch({ message: t('general.errors.noPasswordSet'), status: notificationStatus.DANGER });
      }, 500);
      return { name: 'home' };
    }
  }
};

async function checkIfChatExists(to: any, from: any) {
  const configuration = inject('configuration') as { apiServer: string };
  const result = (await request('GET', `${configuration.apiServer}/api/ai/chat/${to.params.uuid}`)).success;
  if (result) {
    if (to.params.user_uuid === undefined) {
      const uuid = generateUUID();
      return { path: `/chat/${to.params.uuid}/${uuid}` };
    }
    else
      return true;
  }
  else {
    const { t } = i18n.global;
    const baseStore = useBaseStore();
    if (getCookie('Token') !== undefined) {

      setTimeout(() => {
        baseStore.$patch({ message: t("ai.chat.error.doNotExists"), status: notificationStatus.DANGER });
      }, 500);
      return { name: 'overview-chat' };
    } else {
      setTimeout(() => {
        baseStore.$patch({ message: t("ai.chat.error.doNotExists"), status: notificationStatus.DANGER });
      }, 500);
      return { name: 'home' };
    }
  }
};

async function checkIfChatForDownloadExists(to: any, from: any) {
  const configuration = inject('configuration') as { apiServer: string };
  const token = getCookie('Token');
  const chatId = to.query.chatid ?? to.params.chatId;
  const result = (await request('GET', `${configuration.apiServer}/api/ai/chat/${chatId}`, undefined, undefined, false, token !== undefined)).success;
  if (result) {
    return true;
  }
  else {
    const { t } = i18n.global;
    const baseStore = useBaseStore();
    if (getCookie('Token') !== undefined) {

      setTimeout(() => {
        baseStore.$patch({ message: t("ai.chat.error.doNotExists"), status: notificationStatus.DANGER });
      }, 500);
      return { name: 'overview-chat' };
    } else {
      setTimeout(() => {
        baseStore.$patch({ message: t("ai.chat.error.doNotExists"), status: notificationStatus.DANGER });
      }, 500);
      return { name: 'home' };
    }
  }
}

async function checkIfChatForDownloadExistsExporter(to: any, from: any) {
  const configuration = inject('configuration') as { exportApiServer: string };
  const token = getCookie('Token');
  const chatId = to.query.chatid ?? to.params.chatId;
  const result = (await request('GET', `${configuration.exportApiServer}/api/ai/chat/${chatId}`, undefined, undefined, false, token !== undefined)).success;
  if (result) {
    return true;
  }
  else {
    const { t } = i18n.global;
    const baseStore = useBaseStore();
    if (getCookie('Token') !== undefined) {

      setTimeout(() => {
        baseStore.$patch({ message: t("ai.chat.error.doNotExists"), status: notificationStatus.DANGER });
      }, 500);
      return { name: 'overview-chat' };
    } else {
      setTimeout(() => {
        baseStore.$patch({ message: t("ai.chat.error.doNotExists"), status: notificationStatus.DANGER });
      }, 500);
      return { name: 'home' };
    }
  }
}




const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      //@ts-ignore
      component: () => import('../components/general/Base.vue'),
      children: [
        {
          path: '/',
          name: 'home',
          component: IndexView,
          beforeEnter: [goToDashboard],
          children: [
            {
              path: '/impress',
              name: 'impress',
              // route level code-splitting
              // this generates a separate chunk (About.[hash].js) for this route
              // which is lazy-loaded when the route is visited.
              //@ts-ignore
              component: () => import('../views/ImpressView.vue')
            },
            {
              path: '/privacy',
              name: 'privacy',
              // route level code-splitting
              // this generates a separate chunk (About.[hash].js) for this route
              // which is lazy-loaded when the route is visited.
              //@ts-ignore
              component: () => import('../views/PrivacyView.vue')
            },
            {
              path: '/register',
              name: 'registration',
              // route level code-splitting
              // this generates a separate chunk (About.[hash].js) for this route
              // which is lazy-loaded when the route is visited.
              //@ts-ignore
              component: () => import('../views/RegistrationView.vue')
            },
            {
              path: '/register/:id',
              name: 'registration-account',
              // route level code-splitting
              // this generates a separate chunk (About.[hash].js) for this route
              // which is lazy-loaded when the route is visited.
              //@ts-ignore
              component: () => import('../views/AccountCreateView.vue'),
              //beforeEnter: [goToDashboard]
            },
            {
              path: '/password_reset/:id',
              name: 'password-reset',
              // route level code-splitting
              // this generates a separate chunk (About.[hash].js) for this route
              // which is lazy-loaded when the route is visited.
              //@ts-ignore
              component: () => import('../views/PasswordResetView.vue'),
              //beforeEnter: [goToDashboard]
            },
          ]
        },
        {
          path: '/create/:id',
          name: 'copy-pane',
          // route level code-splitting
          // this generates a separate chunk (About.[hash].js) for this route
          // which is lazy-loaded when the route is visited.
          //@ts-ignore
          component: () => import('../views/CreateView.vue'),
          beforeEnter: [checkIfAuthenticated]
        },
        {
          path: '/create',
          name: 'create-pane',
          // route level code-splitting
          // this generates a separate chunk (About.[hash].js) for this route
          // which is lazy-loaded when the route is visited.
          //@ts-ignore
          component: () => import('../views/CreateView.vue'),
          beforeEnter: [checkIfAuthenticated]
        },
        /*{
          path: '/overview/:id',
          name: 'overview',
          // route level code-splitting
          // this generates a separate chunk (About.[hash].js) for this route
          // which is lazy-loaded when the route is visited.
          //@ts-ignore
          component: () => import('../views/UserDashboardView.vue')
        },*/
        {
          path: '/overview',
          name: 'overview-account',
          // route level code-splitting
          // this generates a separate chunk (About.[hash].js) for this route
          // which is lazy-loaded when the route is visited.
          //@ts-ignore
          component: () => import('../views/AccountDashboardView.vue'),
          beforeEnter: [checkIfAuthenticated]
        },
        {
          path: '/overview/chat',
          name: 'overview-chat',
          //@ts-ignore
          component: () => import('../components/chat/Layout.vue'),
          beforeEnter: [checkIfAuthenticated, checkIfAuthorizedForAi],
          children: [
            {
              path: '',
              name: 'chat-overview',
              //@ts-ignore
              component: () => import('../views/chat/AdminChatView.vue'),
            },
            {
              path: 'persons',
              name: 'chat-persons',
              //@ts-ignore
              component: () => import('../views/chat/PersonsOverviewView.vue'),
            },
            {
              path: ':uuid',
              name: 'chat',
              //@ts-ignore
              component: () => import('../views/chat/ChatView.vue'),
            }
          ]
        },
        {
          path: '/overview/chat/dl',
          name: 'chat-download',
          beforeEnter: [checkIfAuthenticated, checkIfAuthorizedForAi, checkIfChatForDownloadExists],
          // route level code-splitting
          // this generates a separate chunk (About.[hash].js) for this route
          // which is lazy-loaded when the route is visited.
          //@ts-ignore
          component: () => import('../views/DownloadView.vue')
        },
        {
          path: 'overview/chat/pdf/:chatId',
          name: 'chat-pdf',
          beforeEnter: [checkIfChatForDownloadExistsExporter],
          // route level code-splitting
          // this generates a separate chunk (About.[hash].js) for this route
          // which is lazy-loaded when the route is visited.
          //@ts-ignore
          component: () => import('../views/chat/PDFView.vue')
        },
        {
          path: 'chat/pdf/:chatId/:user_uuid',
          name: 'user-chat-pdf',
          beforeEnter: [checkIfChatForDownloadExistsExporter],
          // route level code-splitting
          // this generates a separate chunk (About.[hash].js) for this route
          // which is lazy-loaded when the route is visited.
          //@ts-ignore
          component: () => import('../views/chat/PDFView.vue')
        },
        {
          path: '/chat/:uuid',
          name: 'user-chat-entry',
          beforeEnter: [checkIfChatExists],
          children: [
            {
              path: ':user_uuid',
              name: 'user-chat',
              //@ts-ignore
              component: () => import('../views/chat/UserChatView.vue'),
            }
          ]
        },
        {
          path: '/chat/dl',
          name: 'user-chat-download',
          beforeEnter: [checkIfChatForDownloadExists],
          // route level code-splitting
          // this generates a separate chunk (About.[hash].js) for this route
          // which is lazy-loaded when the route is visited.
          //@ts-ignore
          component: () => import('../views/DownloadView.vue')
        },
        {
          path: '/:id',
          name: 'users',
          // route level code-splitting
          // this generates a separate chunk (About.[hash].js) for this route
          // which is lazy-loaded when the route is visited.
          //@ts-ignore
          component: () => import('../views/UserView.vue')
        },
        {
          path: '/a/:id',
          name: 'admin-pane',
          // route level code-splitting
          // this generates a separate chunk (About.[hash].js) for this route
          // which is lazy-loaded when the route is visited.
          //@ts-ignore
          component: () => import('../views/AdminView.vue'),
          beforeEnter: [checkIfAuthenticated]
        },
        {
          path: '/t/:id',
          name: 'mod-pane',
          // route level code-splitting
          // this generates a separate chunk (About.[hash].js) for this route
          // which is lazy-loaded when the route is visited.
          //@ts-ignore
          component: () => import('../views/ModView.vue'),
          beforeEnter: [checkIfExisting]
        },
        {
          path: '/pdf/:id',
          name: 'pdf',
          // route level code-splitting
          // this generates a separate chunk (About.[hash].js) for this route
          // which is lazy-loaded when the route is visited.
          //@ts-ignore
          component: () => import('../views/PDFView.vue')
        },
        {
          path: '/x/:id',
          name: 'export',
          // route level code-splitting
          // this generates a separate chunk (About.[hash].js) for this route
          // which is lazy-loaded when the route is visited.
          //@ts-ignore
          component: () => import('../views/ExportView.vue')
        },
        {
          path: '/p/:id',
          name: 'preview',
          // route level code-splitting
          // this generates a separate chunk (About.[hash].js) for this route
          // which is lazy-loaded when the route is visited.
          //@ts-ignore
          component: () => import('../views/PreviewView.vue')
        },
        {
          path: '/pl/:id',
          name: 'password-less',
          // route level code-splitting
          // this generates a separate chunk (About.[hash].js) for this route
          // which is lazy-loaded when the route is visited.
          //@ts-ignore
          component: () => import('../views/PasswordLessView.vue')
        },
        {
          path: '/dl',
          name: 'download',
          // route level code-splitting
          // this generates a separate chunk (About.[hash].js) for this route
          // which is lazy-loaded when the route is visited.
          //@ts-ignore
          component: () => import('../views/DownloadView.vue')
        },
        /*{
          path: '/password_reset/:id',
          name: 'password-reset',
          // route level code-splitting
          // this generates a separate chunk (About.[hash].js) for this route
          // which is lazy-loaded when the route is visited.
          //@ts-ignore
          component: () => import('../views/PasswordResetView.vue'),
          beforeEnter: [goToDashboard]
        },*/
        {
          path: '/dashboard',
          name: 'statistics',
          // route level code-splitting
          // this generates a separate chunk (About.[hash].js) for this route
          // which is lazy-loaded when the route is visited.
          //@ts-ignore
          component: () => import('../views/StatisticsView.vue')
        },
        {
          path: '/login',
          name: 'login',
          // route level code-splitting
          // this generates a separate chunk (About.[hash].js) for this route
          // which is lazy-loaded when the route is visited.
          //@ts-ignore
          component: () => import('../views/LoginView.vue'),
          beforeEnter: [goToDashboard]
        }
      ],
    }
  ]
})
export default router;