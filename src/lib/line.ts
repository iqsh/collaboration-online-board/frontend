/* HELPER */

/**
 * Calculates the distance between two points in a 2D plane.
 * @param {number} x1 - The x-coordinate of the first point.
 * @param {number} y1 - The y-coordinate of the first point.
 * @param {number} x2 - The x-coordinate of the second point.
 * @param {number} y2 - The y-coordinate of the second point.
 * @returns {number} The distance between the two points.
 */
const distanceBetweenPoints = (x1: any, y1: any, x2: any, y2: any) => {
    return Math.sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2);
};

/**
 * Calculates the midpoints of the edges of a rectangle.
 * @param {Object} rect - The rectangle object with properties x (number), y (number), width (number), and height (number).
 * @returns {Array} An array of objects representing the midpoints of the edges of the rectangle.
 */
const getEdgeMidpoints = (rect: { x: number, y: number, width: number, height: number }) => {
    const midpoints = [];
    midpoints.push({ x: rect.x + rect.width / 2, y: rect.y }); // top
    midpoints.push({ x: rect.x + rect.width / 2, y: rect.y + rect.height }); // bottom
    midpoints.push({ x: rect.x, y: rect.y + rect.height / 2 }); // left
    midpoints.push({ x: rect.x + rect.width, y: rect.y + rect.height / 2 }); // right
    return midpoints;
};

/**
 * Determines the type of edge based on a given midpoint and bounding box.
 * @param {Object} midpoint - The midpoint object with properties x (number) and y (number).
 * @param {Object} bounds - The bounding box object with properties x (number), y (number), width (number), and height (number).
 * @returns {string|undefined} The type of edge ('left', 'right', 'top', 'bottom') or undefined if the midpoint doesn't align with any edge.
 */
const getEdgeTypeFromMidpoint = (midpoint: any, bounds: any) => {
    const threshold = 5; // pixels (adjust as needed)

    // Check horizontal edges (left and right)
    if (Math.abs(midpoint.y - bounds.y - bounds.height / 2) <= threshold) {
        if (midpoint.x < bounds.x + bounds.width / 2) {
            return 'left';
        } else {
            return 'right';
        }
    }

    // Check vertical edges (top and bottom)
    if (Math.abs(midpoint.x - bounds.x - bounds.width / 2) <= threshold) {
        if (midpoint.y < bounds.y + bounds.height / 2) {
            return 'top';
        } else {
            return 'bottom';
        }
    }

    // If midpoint doesn't align with any edge, return undefined
    return undefined;
};


/**
 * Finds the closest edge midpoints between two rectangles.
 * @param {Object} rect1 - The first rectangle object with properties x (number), y (number), width (number), and height (number).
 * @param {Object} rect2 - The second rectangle object with properties x (number), y (number), width (number), and height (number).
 * @param {number} size - The size value used for calculations.
 * @returns {Object} An object containing the closest edge midpoints with properties start and end.
 */
const findClosestEdgeMidpoints = (rect1: any, rect2: any, size: number) => {
    const midpoints1 = getEdgeMidpoints(rect1);
    const midpoints2 = getEdgeMidpoints(rect2);

    let minDistance = Infinity;
    let closestPoints = null;

    const updateClosestPoints = (point1: any, point2: any) => {
        const distance = distanceBetweenPoints(point1.x, point1.y, point2.x, point2.y);
        if (distance < minDistance) {
            minDistance = distance;
            closestPoints = { start: point1, end: point2 };
        }
    };

    const offset = size * 10;
    const validDistance = (typeStart: any, typeEnd: any, point1: any, point2: any) => {
        switch (typeStart) {
            case 'right': return (point1.x < point2.x && (typeEnd === 'bottom' ? point1.y - offset > point2.y : typeEnd === 'top' ? point1.y + offset < point2.y : false)) || (typeEnd === 'left' && point1.x + offset < point2.x);
            case 'left': return (point1.x > point2.x && (typeEnd === 'bottom' ? point1.y - offset > point2.y : typeEnd === 'top' ? point1.y + offset < point2.y : false)) || (typeEnd === 'right' && point1.x - offset > point2.x);
            case 'top': return (point1.y > point2.y && (typeEnd === 'right' ? point1.x - offset > point2.x : typeEnd === 'left' ? point1.x + offset < point2.x : false)) || (typeEnd === 'bottom' && point1.y - offset > point2.y);
            case 'bottom': return (point1.y < point2.y && (typeEnd === 'left' ? point1.x + offset < point2.x : typeEnd === 'right' ? point1.x - offset > point2.x : false)) || (typeEnd === 'top' && point1.y + offset < point2.y);
            default: return false;
        }
    };

    for (const point1 of midpoints1) {
        const typeStart = getEdgeTypeFromMidpoint(point1, rect1);
        for (const point2 of midpoints2) {
            const typeEnd = getEdgeTypeFromMidpoint(point2, rect2);
            if (validDistance(typeStart, typeEnd, point1, point2))
                updateClosestPoints(point1, point2);
        }
    }

    if (closestPoints === null) {
        let start = { x: (rect1.x + rect1.width / 2), y: (rect1.y + rect1.height / 2) }
        let end = { x: (rect2.x + rect2.width / 2), y: (rect2.y + rect2.height / 2) }
        closestPoints = { start: start, end: end };
    }

    return closestPoints;
};

/**
 * Compares two edge types and returns true if they are both horizontal or both vertical.
 * @param {string|undefined} type1 - The first edge type ('left', 'right', 'top', 'bottom') or undefined.
 * @param {string|undefined} type2 - The second edge type ('left', 'right', 'top', 'bottom') or undefined.
 * @returns {boolean|undefined} True if both edge types are horizontal or both are vertical, false if not, undefined if any input is undefined.
 */
const compareEdgeTypes = (type1: string|undefined, type2: string|undefined) => {
    if (type1 === 'left' || type1 === 'right') {
        if (type2 === 'left' || type2 === 'right') {
            return true;
        } else {
            return false;
        }
    } else if (type1 === 'top' || type1 === 'bottom') {
        if (type2 === 'top' || type2 === 'bottom') {
            return true;
        } else {
            return false;
        }
    }
    return undefined;
};

/**
 * Retrieves the position and size of an element relative to its parent.
 * @param {string} elementId - The ID of the element to get position and size for.
 * @returns {Object} An object containing the x, y position and width, height dimensions of the element relative to its parent.
 */
const getElementPosition = (elementId: string) => {
    if (elementId) {
        const element = document.getElementById(elementId);
        if (element) {
            const parent = element.parentElement;
            const elementRect = element.getBoundingClientRect();
            const parentRect = parent!.getBoundingClientRect();
            const x = elementRect.left - parentRect.left;
            const y = elementRect.top - parentRect.top;
            const width = elementRect.width;
            const height = elementRect.height;
            return { x, y, width, height };
        }
    }
    return { x: 0, y: 0, width: 0, height: 0 };
}

/**
* Calculates the SVG path string for a line connecting two points with optional bends.
* @param {Object} startBounds - The bounding box of the starting element {x, y, height, width}.
* @param {Object} endBounds - The bounding box of the ending element {x, y, height, width}.
* @param {number} size - The size factor for the calculation.
* @returns {string} The SVG path string representing the line with optional bends connecting the two points. 
*/
const calculateLinePath = (startBounds: { x: number, y: number, height: number, width: number }, endBounds: { x: number, y: number, height: number, width: number }, size:number) => {
    const closestPoints = findClosestEdgeMidpoints(startBounds, endBounds, size);

    const startMidX = closestPoints!.start.x;
    const startMidY = closestPoints!.start.y;
    const endMidX = closestPoints!.end.x;
    const endMidY = closestPoints!.end.y;

    const twoBends = compareEdgeTypes(getEdgeTypeFromMidpoint({ x: closestPoints!.start.x, y: closestPoints!.start.y }, startBounds), getEdgeTypeFromMidpoint({ x: closestPoints!.end.x, y: closestPoints!.end.y }, endBounds));
    // Calculate the distance between the start and end points
    let deltaX = endMidX - startMidX;
    const direction = deltaX < 0 ? -1 : 1;
    const distance = Math.hypot(endMidX - startMidX, endMidY - startMidY);
    const curvatureFactor = Math.min(distance / 100, 2.00); // Adjust this factor to control the curvature

    // Calculate control points for two bends
    const endX = endBounds.x + endBounds.width / 2;
    const endY = endBounds.y + endBounds.height / 2;
    const arrowAngle = Math.atan2(endY - endMidY, endX - endMidX);

    const midX = (startMidX + endMidX) / 2;
    let controlX1, controlY1, controlX2, controlY2;

    if (twoBends) {
        // Calculate control points for Manhattan-style routing
        if ('1.57' !== arrowAngle.toFixed(2) && '-1.57' !== arrowAngle.toFixed(2)) {

            controlX1 = startMidX + curvatureFactor * (midX - startMidX);
            controlY1 = startMidY;
            controlX2 = endMidX - curvatureFactor * (endMidX - midX);
            controlY2 = endMidY;
        }
        else if ('-1.57' === arrowAngle.toFixed(2)) {
            controlX1 = startMidX;
            controlY1 = startMidY - curvatureFactor * direction * (midX - startMidX);
            controlX2 = endMidX;
            controlY2 = endMidY + curvatureFactor * direction * (endMidX - midX);
        }
        else {
            controlX1 = startMidX;
            controlY1 = startMidY + curvatureFactor * direction * (midX - startMidX);
            controlX2 = endMidX;
            controlY2 = endMidY - curvatureFactor * direction * (endMidX - midX);
        }
    } else {
        const condition = getEdgeTypeFromMidpoint({ x: closestPoints!.start.x, y: closestPoints!.start.y }, startBounds) === 'bottom' || getEdgeTypeFromMidpoint({ x: closestPoints!.start.x, y: closestPoints!.start.y }, startBounds) === 'top';
        if (condition) {
            controlX1 = startMidX;
            controlY1 = endMidY;
        } else {
            controlX1 = endMidX;
            controlY1 = startMidY;
        }
    }

    let path;
    if (twoBends) {
        path = `M ${startMidX} ${startMidY} C ${controlX1} ${controlY1}, ${controlX2} ${controlY2}, ${endMidX} ${endMidY}`;
    } else {
        path = `M ${startMidX} ${startMidY} Q ${controlX1} ${controlY1}, ${endMidX} ${endMidY}`;
    }
    return path;
};

export { calculateLinePath, getElementPosition };