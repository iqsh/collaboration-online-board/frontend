// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import { getCookie } from '@/lib/utils';

/**
 * Sends an asynchronous HTTP request.
 * @param {string} [method='GET'] - The HTTP method for the request.
 * @param {string} [url=''] - The URL to which the request is sent.
 * @param {*} [data=''] - The data to be sent with the request.
 * @param {string} [contentType='application/json'] - The Content-Type for the request.
 * @param {boolean} [isBlob=false] - Indicates if the response is expected as a Blob.
 * @param {boolean} [isSecure=false] - Indicates if the request is secure.
 * @returns {Promise<Response>} - A promise containing the server response.
 */
async function request(method = 'GET', url: string = '', data: any = '', contentType: string = 'application/json', isBlob: boolean = false, isSecure: boolean = false) {
  let options: RequestInit = {
    method: method, // *GET, POST, PUT, DELETE, etc.
    mode: 'cors', // no-cors, *cors, same-origin
    cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
    credentials: 'same-origin', // include, *same-origin, omit

    redirect: 'follow', // manual, *follow, error
    referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
  };
  let token = null;
  if (isSecure) {
    token = getCookie('Token');
  }
  if (method !== 'GET' && method !== 'HEAD' && contentType === 'application/json') {
    options.body = JSON.stringify(data); // body data type must match "Content-Type" header
    options.headers = {
      'Content-Type': contentType,
    };
    if (isSecure) {
      options.headers['Token'] = token;
    }
  }
  else if (method !== 'GET' && method !== 'HEAD' && contentType === 'multipart/form-data') {
    options.body = data;
    if (isSecure) {
      options.headers = {
        'Token': token,
      };
    }
  }
  else {
    if (isSecure) {
      options.headers = {
        'Token': token,
      };
    }
  }
  try {

    const response = await fetch(url, options);
    if (isBlob) {
      return await response.blob();
    } else {
      return await response.json();
    }
  }
  catch (error) {
    return { status: false, error };
  }
}


export { request }