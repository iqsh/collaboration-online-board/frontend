// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import type { uploadPaneTypes } from "@/enums/enums";
import type { line } from "@/types/freeform";
import Compressor from "compressorjs";
import * as jose from 'jose'
import { request } from '@/lib/request';

function getTextWidth(text: string, font: string) {
    // re-use canvas object for better performance

    const canvas: any = document.createElement("canvas");
    const context = canvas.getContext("2d");
    context.font = font;
    const metrics = context.measureText(text);
    return metrics.width;
}

function validMail(mail: string) {
    var regexp = /^[a-zA-Z](([\._\-][a-zA-Z0-9])|[a-zA-Z0-9])*[a-z0-9]$/;
    return regexp.test(mail.split('@')[0]);
};

function validFullMail(mail: string) {
    var regexp = /^[^\s@]+@([^\s@.,]+\.)+[^\s@.,]{2,}$/;
    return regexp.test(mail);

}

/**
 * Move elements in an array
 * @param array {any[]} the array
 * @param oldIndex {number} old index
 * @param newIndex {number} new index
 * @returns {any[]}
 */
function arrayMove(array: any[], oldIndex: number, newIndex: number) {
    if (newIndex >= array.length) {
        var k = newIndex - array.length + 1;
        while (k--) {
            array.push(undefined);
        }
    }
    array.splice(newIndex, 0, array.splice(oldIndex, 1)[0]);
    return array; // for testing
}


/**
 * Deletes all lines, which are connected to a specific element, in HTML + JS and preparation for synchronization
 * @param elementId {string} Name of element which gets deleted
 * @param {*} lineArray {object} lineArray from ShareDB (and JSON) 
 * @returns {{deleteArray: *}} deleteArray as preparation for synchronization
 */
function deleteLines(elementId: string, lineArray: line[]) {
    let deleteArray = [];
    if (lineArray.length > 0) {
        for (var n = lineArray.length - 1, m = 0; n >= m; n--) {
            if (lineArray[n] !== null && lineArray[n] !== undefined) {
                var element = lineArray[n];
                if (element.start === elementId || element.end === elementId) {
                    deleteArray.push({
                        p: ['lineArray', n],
                        ld: element
                    });
                }
            }
        }
    }
    return { deleteArray: deleteArray };
}

/**
 * Compresses an image
 * @param file Image file
 * returns Promise
 */
async function compressImage(file: any) {
    return new Promise((resolve, reject) => {
        new Compressor(file, {
            quality: 0.6,
            convertSize: 0,
            convertTypes: ['image/png', 'image/webp', 'image/jpeg', 'image/avif'],
            maxWidth: 2048,
            maxHeight: 2048,
            success: resolve,
            error: reject
        })
    })
}

async function uploadFile(file: any, hashcode: string, type: uploadPaneTypes, apiServer: string) {
    try {
        const formData = new FormData();
        formData.append('hashcode', hashcode);
        formData.append('t', type);
        if (file.type.includes('image')) {
            const result: any = await compressImage(file);
            formData.append('file', result, result.name);
        } else {
            formData.append('file', file);
        }
        const { success, type: mimetype, path } = await request('POST', apiServer + '/api/file/upload/', formData, 'multipart/form-data');
        if (success) {
            let body = null;
            if (mimetype.includes('image'))
                body = { image: path, type: "file" };
            else if (mimetype.includes('video'))
                body = { video: path, type: "file" };
            else if (mimetype.includes('audio'))
                body = { audio: path, type: "file" };
            else if (mimetype.includes('h5p'))
                body = { h5p: path, type: "file" };
            else
                body = { file: path, type: "file" };
            return { body, mimetype, path }
        } else {
            throw "Not possible" //TODO: 
        }
    } catch (error: any) {
        throw error;
    }
}

function textClear(text: string) {
    return text.replace(/<p><\/p>/g, '').replace(/<p><br class="ProseMirror-trailingBreak"><\/p>/g, '');
}
function checkForURL(text: string) {
    return validUrl(textClear(text).replace(/<p>/g, '').replace(/<\/p>/g, '')) && ([...textClear(text).matchAll(/<p>/g)].length == 1 || [...textClear(text).matchAll(/<p>/g)].length == 0);
}


/**
 * Checks if the url is valid
 * @param {string} url 
 * @returns {boolean} true || false
 */
function validUrl(url: string) {
    let regexp = new RegExp(
        "^(https:\\/\\/)" + // protocol
        "((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|" + // domain name
        "((\\d{1,3}\\.){3}\\d{1,3}))" + // OR ip (v4) address
        "(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*" + // port and path
        "(\\?[;&a-z\\d%_.~+=-]*)?" + // query string
        "(\\#[-a-z\\d_]*)?$",
        "i"
    ); // fragment locator
    return regexp.test(url);
};

/**
 * Converts rgb values to hsl
 * @param {number} r red 
 * @param {number} g green
 * @param {number} b blue
 * @returns hsl as array with [hue, saturation, luminance] 
 */
function rgbToHsl(r: number, g: number, b: number) {
    r /= 255, g /= 255, b /= 255;
    var max = Math.max(r, g, b), min = Math.min(r, g, b);
    var h, s, l = (max + min) / 2;

    if (max == min) {
        h = s = 0; // achromatic
    } else {
        var d = max - min;
        s = l > 0.5 ? d / (2 - max - min) : d / (max + min);
        switch (max) {
            case r: h = (g - b) / d + (g < b ? 6 : 0); break;
            case g: h = (b - r) / d + 2; break;
            case b: h = (r - g) / d + 4; break;
        }
        if (h)
            h /= 6;
    }

    return [h, s, l];
}

/**
 * Makes URLs in text to links in HTML
 * @param {string} text
 * @returns {string} Text with HTML-links
 */
function linkify(text: string) {
    // http://, https://
    var urlPattern =
        // /\b(?:https?):\/\/[a-z0-9-+&@#\/%?=~_|!:,.;]*[a-z0-9-+&@#\/%=~_|]/gim;
        /((https?|ftps?):\/\/[^\"<\s]+)(?![^<>]*>|[^\"]*?<\/a)/gim;

    // www. sans http:// or https://
    var pseudoUrlPattern = /(^|[^\/])(www\.[^\s"<>^`{}]+(\b|$))/gim;


    // Email addresses
    // this regex gets all tags except <p></p>
    //var prepEmail = /[^><\s]*(?:<(?!\/?p(?=>|\s?.*>))\/?.*?>([^><\s]|<(?!\/?p(?=>|\s?.*>))\/?.*?>)*)*@([^><\s]|<(?!\/?p(?=>|\s?.*>))\/?.*?>)*/gim;
    // this regex only gets <span>-tags
    var prepEmail = /([^><\s]|<\/?(span|em|strong)[^>]*>)*@([^><\s]|<\/?(span|em|strong)[^>]*>)*/gim;
    var emailAddressPattern = /[\w._-]+@[a-zA-Z_-]+?(?:\.[a-zA-Z]{2,6})+/gim;
    //var emailAddressPattern =/([^><\s]*?)(?:(?:<span[^>]*>)(\S*?)(?:<\/span>)*)*([\w._-]*@[\w._-]+\.[\w.-]*)/gim

    return text
        .replace(/\\n/g, '<br>')
        .replace(/<p><\/p>/g, '<br>')
        .replace(urlPattern, '<a target="_blank" rel="opener" href="$&">$&</a>')
        .replace(
            pseudoUrlPattern,
            '$1<a class="underline text-blue-900 hover:text-blue-950" target="_blank" rel="opener" href="https://$2">$2</a>'
        )
        .replace(
            prepEmail, function (prep) {
                return prep.replace(/<[^>]*>/gim, '')
            }
        )
        .replace(
            emailAddressPattern,
            '<a class="underline text-blue-900 hover:text-blue-950" target="_blank" rel="opener" href="mailto:$&">$&</a>'
            //'<a target="_blank" rel="opener" href="mailto:$1$2$3">$1$2$3</a>'
        );
}


/**
 * Code from: https://github.com/brankosekulic/trimHtml
 * [trimHtml description]
 * @param  {String} html
 * @param  {Object} options
 * @return {Object}
 */
function trimHtml(html: string, options: any) {
    options = options || {};
    var limit = options.limit || 100,
        preserveTags = (typeof options.preserveTags !== 'undefined') ? options.preserveTags : true,
        wordBreak = (typeof options.wordBreak !== 'undefined') ? options.wordBreak : true,
        suffix = options.suffix || '...',
        moreLink = options.moreLink || '',
        moreText = options.moreText || '»',
        preserveWhiteSpace = options.preserveWhiteSpace || false;
    var arr = html.replace(/</g, "\n<")
        .replace(/>/g, ">\n")
        .replace(/\n\n/g, "\n")
        .replace(/^\n/g, "")
        .replace(/\n$/g, "")
        .split("\n");
    var sum = 0,
        row, cut, add, rowCut,
        tagMatch,
        tagName,
        tagStack = [],
        more = false;
    for (var i = 0; i < arr.length; i++) {
        row = arr[i];
        // count multiple spaces as one character
        if (!preserveWhiteSpace) {
            rowCut = row.replace(/[ ]+/g, ' ');
        } else {
            rowCut = row;
        }
        if (!row.length) {
            continue;
        }
        var charArr = getCharArr(rowCut);
        if (row[0] !== "<") {
            if (sum >= limit) {
                row = "";
            } else if ((sum + charArr.length) >= limit) {
                cut = limit - sum;
                if (charArr[cut - 1] === ' ') {
                    while (cut) {
                        cut -= 1;
                        if (charArr[cut - 1] !== ' ') {
                            break;
                        }
                    }
                } else {
                    add = charArr.slice(cut).indexOf(' ');
                    // break on half of word
                    if (!wordBreak) {
                        if (add !== -1) {
                            cut += add;
                        } else {
                            cut = row.length;
                        }
                    }
                }
                row = charArr.slice(0, cut).join('') + suffix;
                if (moreLink) {
                    row += '<a href="' + moreLink + '" style="display:inline">' + moreText + '</a>';
                }
                sum = limit;
                more = true;
            } else {
                sum += charArr.length;
            }
        } else if (!preserveTags) {
            row = '';
        } else if (sum >= limit) {
            tagMatch = row.match(/[a-zA-Z]+/);
            tagName = tagMatch ? tagMatch[0] : '';
            if (tagName) {
                if (row.substring(0, 2) !== '</') {
                    tagStack.push(tagName);
                    row = '';
                } else {
                    while (tagStack[tagStack.length - 1] !== tagName && tagStack.length) {
                        tagStack.pop();
                    }
                    if (tagStack.length) {
                        row = '';
                    }
                    tagStack.pop();
                }
            } else {
                row = '';
            }
        }
        arr[i] = row;
    }
    return {
        html: arr.join("\n").replace(/\n/g, ""),
        more: more
    };
}

// count symbols like one char
function getCharArr(rowCut: string) {
    var charArr = [],
        subRow,
        match,
        char;
    for (var i = 0; i < rowCut.length; i++) {
        subRow = rowCut.substring(i);
        match = subRow.match(/^&[a-z0-9#]+;/);
        if (match) {
            char = match[0];
            charArr.push(char);
            i += (char.length - 1);
        } else {
            charArr.push(rowCut[i]);
        }
    }
    return charArr;
}

/**
 * Get a cookie by name
 * @param name Name of the cookie
 * @returns Returns a cookie
 */
function getCookie(name: string) {
    let cookie: any = {};
    document.cookie.split(';').forEach(function (el) {
        let [k, v] = el.split('=');
        cookie[k.trim()] = v;
    })
    return cookie[name];
}

/**
 * Sets a cookie 
 * @param token Token from request
 */
function setCookie(token: string, path: string = '/') {
    const payload = jose.UnsecuredJWT.decode(token);
    document.cookie = "Token=" + token + ";" + "expires=" + new Date(Number(payload.payload.exp)).toUTCString() + `;path=${path}`;
    localStorage.setItem('login-event', 'login' + Math.random());
}

/**
 * Deletes a cookie
 * @param name Name of the cookie
 */
function deleteCookie(name: string, path: string = '/') {
    document.cookie = name + `=; Max-Age=-99999999;expires= Wed, 01 Jan 2020 00:00:00 UTC;path=${path}`;
}


/**
 * Checks the file type on invalid types
 * @param accept 
 * @param fileType 
 * @returns boolean
 */
const checkType = (accept: string, fileType: string) => {
    const validMimeTypes = accept.split(',').map((item: string) => item.replace(/\s+/g, '').replace('/*', '').replace('.*', '')).filter((item: string) => item[0] !== '.');
    return validMimeTypes.find((type: string) => fileType.includes(type)) !== undefined;
};


/**
 * Checks the filename on invalid extensions
 * @param accept 
 * @param fileName 
 * @returns boolean
 */
const checkFileExtension = (accept: string, fileName: string) => {
    const validFileExtension = accept.split(',').filter((item: string) => item[0] === '.');
    return validFileExtension.find((ext: string) => fileName.includes(ext));
}

/**
 * Checks if the Browser is Webkit and Safari on iOS
 * @returns boolean
 */
const checkIsSafari = () => {
    const ua = window.navigator.userAgent;
    const isChrome = ua.indexOf('Chrome') > -1;
    const iOS = !!ua.match(/iPad/i) || !!ua.match(/iPhone/i);
    const webkit = !!ua.match(/WebKit/i) && !isChrome;
    const iOSSafari = iOS && webkit && !ua.match(/CriOS/i);
    const iPadOSSafari = webkit && window.navigator.maxTouchPoints > 0;
    return { isWebkit: webkit, isiOSSafari: iOSSafari || iPadOSSafari };
}

const checkIsFirefox = () => {
    /// <reference lib="dom" />
    //@ts-ignore
    const isFirefox = typeof InstallTrigger !== 'undefined';
    return isFirefox;
}

/**
 * Checks if a url is a file
 * @returns string || null
 */
const checkUrlIsFile = (url: string) => {
    if (url.includes(".oga") || url.includes(".aac") || url.includes(".mpga") ||
        url.includes(".wav") || url.includes(".mp3") || url.includes(".flac"))
        return "audio";
    else if (url.includes(".mp4") || url.includes(".webm") || url.includes(".ogg"))
        return "video";
    else if (url.includes(".h5p"))
        return "h5p";
    else if (url.includes(".apng") || url.includes(".avif") || url.includes(".gif") || url.includes(".png") ||
        url.includes(".svg") || url.includes(".webp") || url.includes(".jpg") || url.includes(".jpeg") ||
        url.includes(".jfif") || url.includes(".pjpeg") || url.includes(".pjp"))
        return "image";
    else {
        return null;
    }
}

/**
 * Gets the file type of a post
 * @param body Body of post
 * @param url Url of post
 * @returns "iframe" || "h5p" || "video" || "audio" || "image" || "misc" || null
 */
const getFileType = (body: { iframe?: string, h5p?: string, type?: string, video?: string, image?: string, audio?: string, file?: string }, url: string) => {
    if (body?.iframe) {
        return "iframe";
    }
    if (body?.h5p) {
        return "h5p";
    }
    if (body && body.type === "file" && (body.video || body.image || body.audio)) {
        return body.video ? "video" : body.audio ? "audio" : "image";
    }
    if (url) {
        const res = checkUrlIsFile(url);
        if (!res && body && body.file)
            return "misc";
        return res;
    }
    return null;
}

const convertRemToPixels = (rem: number) => {
    return rem * parseFloat(getComputedStyle(document.documentElement).fontSize);
}

function generateUUID() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = Math.random() * 16 | 0,
            v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}


function isRelativeUrl(str: string) {
    const regex = /^\/[^\/].*|^[^:\/\s?#]+\.[^:\/\s?#]+(?![^\/]*\:\/\/)/;
    return regex.test(str);
}

export { getTextWidth, validMail, validFullMail, arrayMove, deleteLines, uploadFile, validUrl, rgbToHsl, linkify, trimHtml, textClear, getCookie, setCookie, deleteCookie, checkType, checkFileExtension, checkForURL, checkIsSafari, checkUrlIsFile, getFileType, convertRemToPixels, generateUUID, isRelativeUrl, checkIsFirefox }