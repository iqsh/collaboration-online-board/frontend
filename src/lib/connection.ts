// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//@ts-ignore
import ShareDB from 'sharedb-client-browser/dist/sharedb-client-umd.cjs';
import { useBaseStore } from '@/stores/base';
const baseStore = useBaseStore();

export function websocketConnect(url: string, modal: { message: string, ok: string }) {
    let socket: any = new WebSocket(url);
    let heartbeatInterval: any = null;

    function heartbeat() {
        socket.send('{"a":"heartbeat"}');
    }

    socket.addEventListener('open', () => {
        heartbeatInterval = setInterval(heartbeat, 30000);
    });

    socket.addEventListener('close', () => {
        if (heartbeatInterval)
            clearInterval(heartbeatInterval);

        baseStore.updateNotification(modal.message, {
            ok: modal.ok, method: () => {
                location.reload();
            }, modal: true
        });
    });
    return socket;
}

export function connectionInitialize(url: string, modal: { message: string, ok: string }) {
    let socket = websocketConnect(url, modal);
    let connection = new ShareDB.Connection(socket);
    return connection;
}