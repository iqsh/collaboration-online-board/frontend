// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import { defineStore, storeToRefs } from 'pinia';
import type { element, globalStyle, line } from '@/types/freeform';
import { deleteLines } from '@/lib/utils';
import { useMetaStore } from '@/stores/meta';
import { ref, computed } from 'vue';

export const useFreeformStore = defineStore('freeform', () => {
    const lineArray = ref<any[] | null>([]);
    const elementId = ref<string>('');
    const isRnD = ref<boolean>(false);
    const startID = ref<string>('');
    const endID = ref<string>('');
    const lineIndex = ref<number>(-1);
    const lineId = ref<string>('');
    const contextMenuPosition = ref<any>(null);
    const lineObjects = ref<any[]>([]);
    const lineMovingTime = ref<number>(-1);


    const metaStore = useMetaStore();
    const { doc, dataSet, isHeadlineEdit, handleMode, isCanvasOpen, file, showLoadingSpinner, elementTitle, elementText, elementBody, elementInfo, postIndex, showComments, showAdministrationSidebar, commentIndex, elementColor, getDeletable, getMovable, getCommentable, getFullUsable, getLockState, getDataSet, getCount, getStyle, getLikesAllowed, getCommentsAllowed, getEmbeddedsAllowed, getHeadline, getSubHeadline, getHashcode, getNoPassHash, getAdminMail, getDashboardHash, getUserType, openMenu } = storeToRefs(metaStore);

    const getLineArray = computed(() => lineArray.value);
    const getLineTextAllowed = computed(() => doc.value && doc.value.data && doc.value.data.style ? doc.value.data.style.lineText : false);
    const getSize = computed(() => doc.value && doc.value.data ? doc.value.data.size : { max_width: 8000, max_height: 8000 });

 
    function createPost({ color, shape, title, text, body, info, left, top }: { color?: string, shape?: string, title?: string, text: string, body?: string, info?: string, left?: number, top?: number }) {
        return metaStore.createPost({ color, shape, title, text, body, info, left, top });
    }

    function createComment({ postIndex, body, text, type, shape, color, info }: { postIndex: number, body?: any, text: string, type?: string, shape?: string, color?: string, info?: string }) {
        return metaStore.createComment({ index: postIndex, body, text, type, shape, color, info });
    }

    function createLine({ start, end, text, dash, color }: { start: string, end: string, text?: string, dash?: boolean, color?: string }) {
        let identifier = start + end;
        let reverseIdentifier = end + start;
        if (document.getElementById(identifier) === null
            && document.getElementById(reverseIdentifier) === null) {
            let newLine: line = {
                id: identifier, start: start, end: end, text: text,
                dash: dash ? dash : getStyle.value.lineStyle.dash, color: color ? color : getStyle.value.lineStyle.color
            };
            doc.value.submitOp({
                p: ['lineArray', getLineArray.value ? getLineArray.value.length : 0],
                li: newLine
            });
        }
    }

    function editPostPosition({ postIndex, postId, top, left, height, width }: { postIndex?: number, postId?: string, top: string, left: string, height: number, width: number }) {
        let elementIndex = postIndex !== undefined ? postIndex : 0;
        let oldPost = null;
        if (postId) {
            oldPost = doc.value.data.dataSet.find((x: element) => postId == x.id);
            elementIndex = doc.value.data.dataSet.indexOf(oldPost);
        }
        else if (postIndex !== undefined) {
            oldPost = doc.value.data.dataSet[postIndex];
        }
        if (oldPost) {
            let newPost: element = JSON.parse(JSON.stringify(oldPost));
            if (width !== undefined && width !== null && height !== undefined && height !== null)
                newPost.size = { width: width, height: height };
            if (top !== undefined && top !== null && left !== undefined && left !== null)
                newPost.styleObject = { top: top, left: left };
            doc.value.submitOp([{
                p: ['dataSet', elementIndex],
                ld: oldPost,
                li: newPost
            }]);
        }
    }

    function editPostProps({ postIndex, postId, text, title, info, body, color, shape, height, width, visible, authorized }: { postIndex?: number, postId?: string, text?: string, title?: string, info?: string, body?: any, color?: string, shape?: string, height?: number, width?: number, visible?: boolean, authorized?: boolean }) {
        return metaStore.editPostProps({ id: postId, index: postIndex, text, title, color, shape, info, body, visible, authorized, height, width });
    }

    function likePost() {
        //TODO: to be designed
    }

    function editPostZAxis({ postIndex, postId, direction }: { postIndex?: number, postId?: string, direction: string }) {
        let elementIndex = postIndex !== undefined ? postIndex : 0;
        if (postId) {
            elementIndex = doc.value.data.dataSet.findIndex((x: element) => postId === x.id);
        }
        let frontIndex = doc.value.data.dataSet.length - 1;
        if (direction === 'back' && elementIndex > 0) {
            doc.value.submitOp([{
                p: ['dataSet', elementIndex],
                lm: 0
            }]);
        } else if (direction === 'front' && elementIndex < frontIndex) {
            doc.value.submitOp([{
                p: ['dataSet', elementIndex],
                lm: frontIndex
            }]);
        }
    }

    function editCommentPosition({ postIndex, postId, commentIndex, direction }: { postIndex?: number, postId?: string, commentIndex: number, direction: string }) {
        metaStore.editCommentPosition({ index: postIndex, id: postId, commentIndex, direction });
    }

    function editCommentProps({ postIndex, postId, commentIndex, text, info, body, type, color, shape, visible }: { postIndex?: number, postId?: string, commentIndex: number, text?: string, info?: string, body?: string, type?: string, color?: string, shape?: string, visible?: boolean }) {
        metaStore.editCommentProps({ index: postIndex, id: postId, commentIndex, body, text, shape, color, info, visible, type });
    }

    function likeComment({ postIndex, postId, commentIndex }: { postIndex?: number, postId?: string, commentIndex: number }) {
        metaStore.likeComment({ index: postIndex, id: postId, commentIndex });
    }

    function editHeadlines({ headline, subheadline }: { headline: string, subheadline: string }) {
        metaStore.editHeadlines({ headline, subheadline });
    }

    function editStyle(style: globalStyle) {
        metaStore.editStyle(style);
    }

    function editRight({ type, value }: { type: string, value: boolean }) {
        metaStore.editRight({ type, value });
    }

    function editLine({ lineIndex, lineId, text }: { lineIndex?: number, lineId?: string, text: string }) {
        let elementIndex = lineIndex !== null ? lineIndex : 0;
        let lineObject = null;
        if (lineId) {
            lineObject = doc.value.data.lineArray.find((x: any) => x.id = lineId);
            elementIndex = doc.value.data.lineArray.indexOf(lineObject);
        } else if (lineIndex !== null && lineIndex !== undefined)
            lineObject = doc.value.data.lineArray[lineIndex];
        if (lineObject) {
            let newLineObject: line = JSON.parse(JSON.stringify(lineObject));
            newLineObject.text = text;
            doc.value.submitOp({
                p: ['lineArray', elementIndex],
                ld: lineObject,
                li: newLineObject
            });
        }
    }



    function deletePost({ postIndex, postId }: { postIndex?: number, postId?: string }) {
        if (postId || (postIndex !== undefined)) {
            let elementIndex = postIndex !== undefined ? postIndex : 0;
            let oldPost = null;
            if (postId && postIndex === undefined) {
                oldPost = doc.value.data.dataSet.find((x: element) => postId == x.id);
                elementIndex = doc.value.data.dataSet.findIndex((x: element) => postId == x.id);
            }
            else if (postIndex !== undefined) {
                oldPost = doc.value.data.dataSet[postIndex];
            }
            let oldPostId = postId !== undefined ? postId : oldPost.id;
            if (oldPost) {
                let newPost: element = JSON.parse(JSON.stringify(oldPost));
                let deleteArray: any[] = deleteLines(oldPostId, doc.value.data.lineArray).deleteArray;
                let deleteObject = { id: postId };
                deleteArray.push({
                    p: ['deleteObject'],
                    oi: deleteObject
                });
                deleteArray.push({
                    p: ['dataSet', elementIndex],
                    ld: newPost,
                });
                doc.value.submitOp(deleteArray);
                return { post: newPost.title.post, comments: newPost.comments };
            }
        }
        return false;
    }

    function deleteComment({ postIndex, postId, commentIndex }: { postIndex?: number, postId?: string, commentIndex: number }) {
        metaStore.deleteComment({ index: postIndex, id: postId, commentIndex });
    }

    function deleteLine({ lineIndex, lineId }: { lineIndex?: number, lineId?: string }) {
        let elementIndex = lineIndex !== undefined ? lineIndex : 0;
        let object = null;
        if (lineId) {
            object = doc.value.data.lineArray.find((x: any) => x.id == lineId);
            elementIndex = doc.value.data.lineArray.indexOf(object);
        }
        else if (lineIndex !== undefined)
            object = doc.value.data.lineArray[lineIndex];
        if (object) {
            doc.value.submitOp([{
                p: ['lineArray', elementIndex],
                ld: object
            }]);
        }
    }

    function setCount() {
        doc.value.submitOp([{ p: ['count'], na: 1 }]);
    }

    return {
        doc,
        dataSet,
        lineArray,
        isHeadlineEdit,
        handleMode,
        isCanvasOpen,
        file,
        elementId,
        isRnD,
        showLoadingSpinner,
        startID,
        endID,
        elementTitle,
        elementText,
        elementBody,
        elementInfo,
        lineIndex,
        lineId,
        contextMenuPosition,
        lineObjects,
        postIndex,
        lineMovingTime,
        showAdministrationSidebar,
        commentIndex,
        elementColor,
        getDeletable,
        getMovable,
        getCommentable,
        getFullUsable,
        getLockState,
        getCount,
        getLineArray,
        getDataSet,
        getStyle,
        getLikesAllowed,
        getCommentsAllowed,
        getEmbeddedsAllowed,
        getLineTextAllowed,
        getHeadline,
        getSubHeadline,
        getSize,
        getHashcode,
        getNoPassHash,
        getAdminMail,
        getDashboardHash,
        getUserType,
        showComments,
        openMenu,
        createPost,
        createComment,
        createLine,
        editPostPosition,
        editPostProps,
        likePost,
        editPostZAxis,
        editCommentPosition,
        editCommentProps,
        likeComment,
        editHeadlines,
        editStyle,
        editLine,
        editRight,
        deletePost,
        deleteComment,
        deleteLine,
        setCount
    };
});
