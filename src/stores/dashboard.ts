// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import { handleModeEnum, paneTypes, sortEnum } from '@/enums/enums';
import type { dashboardItem, folder } from '@/types/dashboard';
import { defineStore } from 'pinia';
import { ref } from 'vue';

export const useDashboardStore = defineStore('dashboard', () => {
    const folderId = ref<string>('');
    const folderName = ref<string>('');
    const folderColor = ref<string>('#ffffff');
    const folderParent = ref<string>('');
    const paneId = ref<string>('');
    const handleMode = ref<handleModeEnum>(handleModeEnum.NONE);
    const folders = ref<folder[] | undefined>(undefined);
    const allFolders = ref<folder[] | undefined>(undefined);
    const currentFolder = ref<string>('');
    const paneHandleMode = ref<handleModeEnum>(handleModeEnum.NONE);
    const isMoveToFolderModalOpen = ref<boolean>(false);
    const selectedPaneType = ref<paneTypes>(paneTypes.ALL);
    const selectedSort = ref<sortEnum>(sortEnum.DESC);
    const favorites = ref<dashboardItem[]>([]);
    const select = ref<boolean>(false);
    const selectedItems = ref<String[]>([]);

    function reset() {
        folderId.value = '';
        folderName.value = '';
        folderColor.value = '#ffffff';
        folderParent.value = '';
        paneId.value = '';
        handleMode.value = handleModeEnum.NONE;
        select.value = false;
        selectedItems.value = [];
    }

    return {
        folderId,
        folderName,
        folderColor,
        folderParent,
        paneId,
        handleMode,
        folders,
        allFolders,
        currentFolder,
        paneHandleMode,
        isMoveToFolderModalOpen,
        selectedPaneType,
        selectedSort,
        favorites,
        select,
        selectedItems,
        reset
    };
});
