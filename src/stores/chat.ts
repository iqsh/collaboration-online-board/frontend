// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later
import { defineStore } from 'pinia';
import { ref, inject } from 'vue';
import { request } from '@/lib/request';

export const useChatStore = defineStore('chat', () => {
    const configuration = inject<any>('configuration');
    const userType = ref<string>('');
    const tab = ref<string>('chat');
    const chats = ref<any[]>([]);
    const tokensUsed = ref<number>(0);
    const tokenQuota = ref<number>(0);
    const state = ref<string>('');
    const uuid = ref<string>(''); // only or create or edit of chat or person
    const username = ref<string>('');
    const chatTitle = ref<string>('');
    const activeChatUuid = ref<string>('');
    const isShared = ref<boolean>(true);
    const loadMessage = ref<boolean>(false);
    const loadImage = ref<boolean>(false);

    const reloadMessages = ref<boolean>(false);

    const updateChatTitle = (newTitle: string) => {
        chatTitle.value = newTitle;
    };

    

    const getChats = async () => {
        const result = await request('GET', configuration.apiServer + "/api/ai/chat/", undefined, undefined, false, true);
        if (result.success) {
            chats.value = result.chats;
        }
    };

    return {
        userType,
        tab,
        chats,
        tokensUsed,
        tokenQuota,
        state,
        uuid,
        username,
        chatTitle,
        activeChatUuid,
        isShared,
        loadMessage,
        loadImage,
        reloadMessages,
        updateChatTitle,
        getChats,
    };
});
