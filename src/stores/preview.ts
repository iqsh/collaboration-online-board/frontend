// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import { paneTypes } from '@/enums/enums';
import { defineStore } from 'pinia';
import { ref } from 'vue';

export const usePreviewStore = defineStore('preview', () => {
    const paneType = ref<paneTypes>(paneTypes.NONE);
    const hashcode = ref<string>('');
    const previewHashcode = ref<string>('');
    const adminMail = ref<string>('');
    const data = ref<any>(null);
    const lineArray = ref<any>(null);
    const style = ref<any>(null);
    const columnIndex = ref<number>(-1);
    const postIndex = ref<number>(-1);
    const commentsAllowed = ref<boolean>(false);
    const likesAllowed = ref<boolean>(false);
    const headline = ref<string>('');
    const subheadline = ref<string>('');
    const accesscode = ref<string>('');
    const mail = ref<string>('');
    const mailValid = ref<boolean>(true);
    const isExport = ref<boolean>(false);

    return {
        paneType,
        hashcode,
        previewHashcode,
        adminMail,
        data,
        lineArray,
        style,
        columnIndex,
        postIndex,
        commentsAllowed,
        likesAllowed,
        headline,
        subheadline,
        accesscode,
        mail,
        mailValid,
        isExport
    };
});
