// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import { defineStore, storeToRefs } from 'pinia';
import type { element, globalStyle } from '@/types/timeline';
import { useMetaStore } from '@/stores/meta';
import { ref, computed } from 'vue';

export const useTimelineStore = defineStore('timeline', () => {
    const elementId = ref<string>('');
    const contextMenuPosition = ref<any>(null);

    const metaStore = useMetaStore();
    const { doc, dataSet, isHeadlineEdit, handleMode, isCanvasOpen, file, showLoadingSpinner, elementTitle, elementText, elementBody, elementInfo, postIndex, showComments, showAdministrationSidebar, commentIndex, elementColor, getDeletable, getMovable, getCommentable, getFullUsable, getLockState, getDataSet, getCount, getStyle, getLikesAllowed, getCommentsAllowed, getEmbeddedsAllowed, getHeadline, getSubHeadline, getHashcode, getNoPassHash, getAdminMail, getDashboardHash, getUserType, openMenu } = storeToRefs(metaStore);

    const getLineSize = computed(() => doc.value && doc.value.data && doc.value.data.style ? Number(doc.value.data.style.lineSize) : 2);
    const getLineColor = computed(() => doc.value && doc.value.data && doc.value.data.style ? `${doc.value.data.style.lineColor}80` : "#66666680");
    const getLineElementsColor = computed(() => doc.value && doc.value.data && doc.value.data.style ? doc.value.data.style.lineColor : "#666666");

    function createPost({ color, shape, title, text, body, info, index }:
        { color?: string, shape?: string, title?: string, text: string, body?: string, info?: string, index?: number }) {
        return metaStore.createPost({ color, shape, title, text, body, info, index });
    }

    function createComment({ postIndex, body, text, type, shape, color, info }:
        { postIndex: number, body?: any, text: string, type?: string, shape?: string, color?: string, info?: string }) {
            return metaStore.createComment({ index:postIndex, body, text, type, shape, color, info });
    }

    function editPostPosition({ postIndex, postId, newPostIndex, direction }: { postIndex: number, postId?: string, newPostIndex?: number, direction?: string }) {
        let newElementIndex = newPostIndex !== undefined ? newPostIndex : 0;
        let elementIndex = postIndex !== undefined ? postIndex : 0;
        let oldPost = null;
        if (postId) {
            oldPost = doc.value.data.dataSet.find((x: element) => postId == x.id);
            elementIndex = doc.value.data.dataSet.indexOf(oldPost);
        }
        else if (postIndex !== undefined) {
            oldPost = doc.value.data.dataSet[postIndex];
        }
        if (newPostIndex === undefined) {
            newPostIndex = postIndex;
            let arrayLength = doc.value.data.dataSet.length;
            if (postIndex > 0 && (postIndex < arrayLength - 1)) {
                newElementIndex = direction === "+1" ? (Number(postIndex) + 1) : (Number(postIndex) - 1);
            } else if (postIndex === 0) {
                newElementIndex = direction === "+1" ? newPostIndex + 1 : newPostIndex;
            } else if (postIndex === arrayLength - 1) {
                newElementIndex = direction === "+1" ? newPostIndex : newPostIndex - 1;
            } else {
                newElementIndex = postIndex;
            }
        }
        if (oldPost) {
            doc.value.submitOp({
                p: ['dataSet', elementIndex],
                lm: newElementIndex
            });
        }
    }

    function editPostProps({ postIndex, postId, text, title, info, body, color, shape, height, width, visible, authorized }:
        { postIndex?: number, postId?: string, text?: string, title?: string, info?: string, body?: any, color?: string, shape?: string, height?: number, width?: number, visible?: boolean, authorized?: boolean }) {
            return metaStore.editPostProps({ id:postId, index:postIndex, text, title, color, shape, info, body, visible, authorized, height, width });
    }

    function likePost({ postIndex, postId }: { postIndex?: number, postId?: string }) {
        metaStore.likePost({index:postIndex, id:postId});
    }

    function editCommentPosition({ postIndex, postId, commentIndex, direction }: { postIndex?: number, postId?: string, commentIndex: number, direction: string }) {
        metaStore.editCommentPosition({ index:postIndex, id:postId, commentIndex, direction });
    }

    function editCommentProps({ postIndex, postId, commentIndex, text, info, body, type, color, shape, visible }:
        { postIndex?: number, postId?: string, commentIndex: number, text?: string, info?: string, body?: string, type?: string, color?: string, shape?: string, visible?: boolean }) {
        metaStore.editCommentProps({ index:postIndex, id:postId, commentIndex, body, text, shape, color, info, visible, type });
    }

    function likeComment({ postIndex, postId, commentIndex }: { postIndex?: number, postId?: string, commentIndex: number }) {
        metaStore.likeComment({ index:postIndex, id:postId, commentIndex });
    }

    function editHeadlines({ headline, subheadline }: { headline: string, subheadline: string }) {
        metaStore.editHeadlines({ headline, subheadline });
    }

    function editStyle(style: globalStyle) {
        metaStore.editStyle(style);
    }

    function editRight({ type, value }: { type: string, value: boolean }) {
        metaStore.editRight({type, value});
    }

    function deletePost({ postIndex, postId }: { postIndex?: number, postId?: string }) {
        if (postId || (postIndex !== undefined)) {
            let elementIndex = postIndex !== undefined ? postIndex : 0;
            let oldPost = null;
            if (postId && postIndex === undefined) {
                oldPost = doc.value.data.dataSet.find((x: element) => postId == x.id);
                elementIndex = doc.value.data.dataSet.findIndex((x: element) => postId == x.id);
            }
            else if (postIndex !== undefined) {
                oldPost = doc.value.data.dataSet[postIndex];
            }
            if (oldPost) {
                let newPost: element = JSON.parse(JSON.stringify(oldPost));
                doc.value.submitOp({
                    p: ['dataSet', elementIndex],
                    ld: newPost,
                });
                return { post: newPost.title.post, comments: newPost.comments };
            }
        }
        return false;
    }

    function deleteComment({ postIndex, postId, commentIndex }: { postIndex?: number, postId?: string, commentIndex: number }) {
        metaStore.deleteComment({index:postIndex, id:postId, commentIndex});
    }

    function setCount() {
        doc.value.submitOp([{ p: ['count'], na: 1 }]);
    }

    return {
        doc,
        dataSet,
        isHeadlineEdit,
        handleMode,
        isCanvasOpen,
        file,
        elementId,
        showLoadingSpinner,
        elementTitle,
        elementText,
        elementBody,
        elementInfo,
        contextMenuPosition,
        postIndex,
        showAdministrationSidebar,
        showComments,
        commentIndex,
        elementColor,
        getDeletable,
        getMovable,
        getCommentable,
        getFullUsable,
        getLockState,
        getCount,
        getDataSet,
        getStyle,
        getLikesAllowed,
        getCommentsAllowed,
        getEmbeddedsAllowed,
        getHeadline,
        getSubHeadline,
        getHashcode,
        getNoPassHash,
        getAdminMail,
        getDashboardHash,
        getUserType,
        getLineSize,
        getLineColor,
        getLineElementsColor,
        openMenu,
        createPost,
        createComment,
        editPostPosition,
        editPostProps,
        likePost,
        editCommentPosition,
        editCommentProps,
        likeComment,
        editHeadlines,
        editStyle,
        editRight,
        deletePost,
        deleteComment,
        setCount
    };
});
