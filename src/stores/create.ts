// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import { defineStore } from 'pinia';
import { ref } from 'vue';

export const useCreateStore = defineStore('create', () => {
    const type = ref<string>("Schedule");
    const uPassword = ref<string>("");
    const uPasswordValid = ref<boolean>(true);
    const aPassword = ref<string>("");
    const aPasswordValid = ref<boolean>(true);
    const mail = ref<string>("");
    const accesscode = ref<string>("");
    const hashcode = ref<string>("");
    const isCreate = ref<boolean>(true);
    const activePage = ref<string>("account");
    const headline = ref<string>("");
    const subheadline = ref<string>("");

    const elementColor = ref<string>("#ffffff");
    const elementShape = ref<string>("rectangle");
    const commentColor = ref<string>("#ffffff");
    const commentShape = ref<string>("rectangle");
    const likeIcon = ref<string>("heart");
    const allowComments = ref<boolean>(true);
    const allowEmbeddeds = ref<boolean>(true);
    const allowLikes = ref<boolean>(true);
    const fontSize = ref<string>("uk-text-default");
    const backgroundColor = ref<string>("#ffffff");
    const font = ref<string>("default");

    const lineSize = ref<number>(1);
    const arrowEnding = ref<string>("none");
    const lineColor = ref<string>("black");
    const lineStyle = ref<string>("solid");
    const lineText = ref<boolean>(true);

    const columnWidth = ref<string>('250');
    const oneColumn = ref<boolean>(false);
    const columnAddPosition = ref<string>('bottom');

    const elementWidth = ref<string>('250');

    const reset = async ()=>{
        type.value = "Schedule";
        uPassword.value = "";
        uPasswordValid.value = true;
        aPassword.value = "";
        aPasswordValid.value = true;
        mail.value = "";
        accesscode.value = "";
        hashcode.value = "";
        isCreate.value = true;
        activePage.value = "account";
        headline.value = "";
        subheadline.value = "";
        elementColor.value = "#ffffff";
        elementShape.value = "rectangle";
        commentColor.value = "#ffffff";
        commentShape.value = "rectangle";
        likeIcon.value = "heart";
        allowComments.value = true;
        allowEmbeddeds.value = true;
        allowLikes.value = true;
        fontSize.value = "uk-text-default";
        backgroundColor.value = "#ffffff";
        font.value = "default";
        lineSize.value = 1;
        arrowEnding.value = "none";
        lineColor.value = "black";
        lineStyle.value = "solid";
        lineText.value = true;
        columnWidth.value = "250";
        oneColumn.value = false;
        columnAddPosition.value = "bottom";
        elementWidth.value = "250";
    };

    return {
        type,
        uPassword,
        uPasswordValid,
        aPassword,
        aPasswordValid,
        mail,
        accesscode,
        hashcode,
        isCreate,
        activePage,
        headline,
        subheadline,
        elementColor,
        elementShape,
        commentColor,
        commentShape,
        likeIcon,
        allowComments,
        allowEmbeddeds,
        allowLikes,
        fontSize,
        backgroundColor,
        font,
        lineSize,
        arrowEnding,
        lineColor,
        lineStyle,
        lineText,
        columnWidth,
        oneColumn,
        columnAddPosition,
        elementWidth,
        reset
    };
});
