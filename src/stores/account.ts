// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import { defineStore } from 'pinia';
import { ref } from 'vue';

export const useAccountStore = defineStore('account', () => {
    const mail = ref<string>('');
    const accesscode = ref<string>('');
    const canUseAI = ref<boolean>(false);

    return {
        mail,
        accesscode,
        canUseAI
    };
});