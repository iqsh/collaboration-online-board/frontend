// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import { notificationStatus } from '@/enums/enums';
import { defineStore } from 'pinia';
import { ref } from 'vue';

export interface BaseState {
    status: notificationStatus;
    message: string;
    modal: boolean;
    method: ($event?: undefined) => any;
    modalOKLabel: string;
}

export const useBaseStore = defineStore('base', () => {
    const status = ref<notificationStatus>(notificationStatus.SUCCESS);
    const message = ref<string>('');
    const modal = ref<boolean>(false);
    const method = ref<($event?: undefined) => any>(() => {});
    const modalOKLabel = ref<string>('');

    function updateNotification(msg: string, options?: {status?: notificationStatus, method?: ($event?: undefined) => any, ok?: string, modal?: boolean}) {
        message.value = msg;
        if (options) {
            if (options.status) {
                status.value = options.status;
            }
            if (options.method) {
                method.value = options.method;
            }
            if (options.ok) {
                modalOKLabel.value = options.ok;
            }
            if (options.modal) {
                modal.value = options.modal;
            }
        }
    }

    return {
        status,
        message,
        modal,
        method,
        modalOKLabel,
        updateNotification
    };
});
