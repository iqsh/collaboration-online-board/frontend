// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import { defineStore, storeToRefs } from 'pinia';
import { userType } from '@/enums/enums';
import type { column, globalStyle } from '@/types/schedule';
import { arrayMove } from '@/lib/utils';
import { useMetaStore } from '@/stores/meta';
import { ref, computed } from 'vue';

export const useScheduleStore = defineStore('schedule', () => {
    const columnIndex = ref<number>(-1);
    const movePostIndex = ref<number>(-1);
    const moveColumnIndex = ref<number>(-1);
    const openColumnId = ref<string>('');
    const openPostId = ref<string>('');
    const insertAtIndex = ref<number>(-1);

    const metaStore = useMetaStore();
    const { doc, dataSet, isHeadlineEdit, handleMode, isCanvasOpen, file, showLoadingSpinner, elementTitle, elementText, elementBody, elementInfo, postIndex, showComments, showAdministrationSidebar, commentIndex, elementColor, getDeletable, getMovable, getCommentable, getFullUsable, getLockState, getDataSet, getCount, getStyle, getLikesAllowed, getCommentsAllowed, getEmbeddedsAllowed, getHeadline, getSubHeadline, getHashcode, getNoPassHash, getAdminMail, getDashboardHash, getUserType, openMenu } = storeToRefs(metaStore);

    const getHeaderEditable = computed(() => metaStore.userType !== userType.USER ? true : doc.value && doc.value.data ? doc.value.data.unlocked.all && doc.value.data.unlocked.editHeader : false);
    const getOneColumn = computed(() => doc.value && doc.value.data && doc.value.data.style ? doc.value.data.style.oneColumn : false);
    const getColumnAddPosition = computed(() => doc.value && doc.value.data && doc.value.data.style?.columnAddPosition ? doc.value.data.style.columnAddPosition : "bottom");


    function createColumn({ text, color, shape }: { text: string, color?: string, shape?: string }) {
        let id = "Column_" + (+new Date).toString(28) + Math.random().toString(36).substring(2, 9);
        let posts = Array();
        let column: column = {
            id: id, title: text, postLength: 0, posts: posts, shape: shape ? shape : getStyle.value.shape,
            color: color ? color : getStyle.value.shapeColor, visible: true
        };
        const index = insertAtIndex.value !== -1 ? 0 : getDataSet.value?.length ?? 0;
        doc.value.submitOp([{
            p: ['dataSet', index],
            li: column
        }, {
            p: ['count'],
            na: 1
        }]);
        return id;
    }

    function createPost({ columnId, columnIndex, text, title, color, shape, info, body, index }:
        { columnId?: string, columnIndex?: number, text: string, title?: string, color?: string, shape?: string, info?: string, body?: string, index?: number }) {
        return metaStore.createPost({columnId, columnIndex, text, title, color, shape, info, body, index});
    }

    function createComment({ columnIndex, postIndex, body, text, type, shape, color, info }:
        {
            columnIndex: number, postIndex: number, body?: any, text: string, type?: string,
            shape?: string, color?: string, info?: string
        }) {
        return metaStore.createComment({ index:columnIndex, postIndex, body, text, type, shape, color, info });
    }

    function editColumnProps({ columnId, columnIndex, text, color, shape, visible }: { columnId?: string, columnIndex?: number, text?: string, color?: string, shape?: string, visible?: boolean }) {
        let elementIndex = columnIndex !== undefined ? columnIndex : 0;
        let oldColumn = null;
        if (columnId) {
            oldColumn = doc.value.data.dataSet.find((x: column) => columnId == x.id);
            elementIndex = doc.value.data.dataSet.indexOf(oldColumn);
        }
        else if (columnIndex !== undefined) {
            oldColumn = doc.value.data.dataSet[columnIndex];
        }
        if (oldColumn) {
            let newColumn = JSON.parse(JSON.stringify(oldColumn));
            if (text)
                newColumn.title = text;
            if (color)
                newColumn.color = color;
            if (shape)
                newColumn.shape = shape;
            if (visible !== undefined)
                newColumn.visible = visible;
            doc.value.submitOp({
                p: ['dataSet', elementIndex],
                ld: oldColumn,
                li: newColumn
            });
        }
    }

    function editColumnPosition({ columnIndex, newColumnIndex, direction }: { columnIndex: number, newColumnIndex?: number, direction?: string }) {
        let column: column = doc.value.data.dataSet[columnIndex];
        let newElementIndex = newColumnIndex !== undefined ? newColumnIndex : 0;
        if (newColumnIndex === undefined) {
            newColumnIndex = columnIndex;
            let arrayLength = doc.value.data.dataSet.length;
            if (columnIndex > 0 && (columnIndex < arrayLength - 1)) {
                newElementIndex = direction === "+1" ? (Number(columnIndex) + 1) : (Number(columnIndex) - 1);
            } else if (columnIndex === 0) {
                newElementIndex = direction === "+1" ? newColumnIndex + 1 : newColumnIndex;
            } else if (columnIndex === arrayLength - 1) {
                newElementIndex = direction === "+1" ? newColumnIndex : newColumnIndex - 1;
            } else {
                newElementIndex = columnIndex;
            }
        }
        if (column) {
            doc.value.submitOp({
                p: ['dataSet', columnIndex],
                lm: newElementIndex
            });
        }
    }

    function editPostProps({ columnId, columnIndex, postIndex, text, title, color, shape, info, body, visible, authorized }:
        { columnId?: string, columnIndex?: number, postIndex: number, text?: string, title?: string, color?: string, shape?: string, info?: string, body?: string, visible?: boolean, authorized?: boolean }) {
        return metaStore.editPostProps({ id:columnId, index:columnIndex, postIndex, text, title, color, shape, info, body, visible, authorized });
    }

    function editPostPosition({ columnIndex, postIndex, direction, newColumnIndex, newPostIndex }: { columnIndex: number, postIndex: number, direction?: string, newColumnIndex?: number, newPostIndex?: number }) {
        let oldColumn = doc.value.data.dataSet[columnIndex];
        if (oldColumn && newColumnIndex === undefined && (direction !== undefined || newPostIndex !== undefined)) {
            let newColumn: column = Object.assign({}, oldColumn);
            let newElementIndex = newPostIndex !== undefined ? newPostIndex : postIndex;
            let posts = newColumn.posts;
            if (direction !== undefined) {
                let arrayLength = posts.length;
                if (postIndex > 0 && (postIndex < arrayLength - 1))
                    newElementIndex = direction === "+1" ? postIndex + 1 : postIndex - 1;
                else if (postIndex === 0)
                    newElementIndex = direction === "+1" ? postIndex + 1 : postIndex;
                else if (postIndex === arrayLength - 1)
                    newElementIndex = direction === "+1" ? postIndex : postIndex - 1;
            }
            posts = arrayMove(posts, postIndex, newElementIndex);
            newColumn.posts = posts;
            doc.value.submitOp({
                p: ['dataSet', columnIndex],
                ld: oldColumn,
                li: newColumn
            });
        } else if (oldColumn && newColumnIndex !== undefined) {
            let post = oldColumn.posts[postIndex];
            let newFirstColumn: column = Object.assign({}, oldColumn);
            newFirstColumn.posts.splice(postIndex, 1);
            let oldSecondColumn: column = Object.assign({}, doc.value.data.dataSet[newColumnIndex]);
            let newSecondColumn: column = Object.assign({}, oldSecondColumn);
            post.id = "post_" + newColumnIndex + "_" + newSecondColumn.postLength + (+new Date).toString(28) + Math.random().toString(36).substring(2, 9);
            const newPosition = newSecondColumn.posts.push(post);
            newSecondColumn.postLength = newSecondColumn.postLength + 1;
            const posts = newSecondColumn.posts;
            if(newPostIndex !== undefined && newPostIndex < posts.length-1) {
                newSecondColumn.posts = arrayMove(posts, newPosition-1, newPostIndex);
            }
            doc.value.submitOp([{
                p: ['dataSet', newColumnIndex],
                ld: oldSecondColumn,
                li: newSecondColumn
            }, {
                p: ['dataSet', columnIndex],
                ld: oldColumn,
                li: newFirstColumn
            }]);
        }
    }
    function likePost({ columnIndex, postIndex }: { columnIndex: number, postIndex: number }) {
        metaStore.likePost({index:columnIndex, postIndex});
    }

    async function editCommentProps({ columnIndex, postIndex, commentIndex, body, text, shape, color, info, visible }:
        {
            columnIndex: number, postIndex: number, commentIndex: number, body?: any, text: string
            shape?: string, color?: string, info?: string, visible?: boolean
        }) {
        metaStore.editCommentProps({ index:columnIndex, postIndex, commentIndex, body, text, shape, color, info, visible });
    }

    function editCommentPosition({ columnIndex, postIndex, commentIndex, direction }: { columnIndex: number, postIndex: number, commentIndex: number, direction: string }) {
        metaStore.editCommentPosition({ index:columnIndex, postIndex, commentIndex, direction });
    }

    
    function likeComment({ columnIndex, postIndex, commentIndex }: { columnIndex: number, postIndex: number, commentIndex: number }) {
        metaStore.likeComment({ index:columnIndex, postIndex, commentIndex });
    }

    function editHeadlines({ headline, subheadline }: { headline: string, subheadline: string }) {
        metaStore.editHeadlines({ headline, subheadline });
    }

    function editStyle(style: globalStyle) {
        metaStore.editStyle(style);
    }

    function editRight({ type, value }: { type: string, value: boolean }) {
        metaStore.editRight({type, value});
    }

    function deleteColumn({ columnIndex }: { columnIndex: number }) {
        let column = doc.value.data.dataSet[columnIndex];
        if (column) {
            doc.value.submitOp({
                p: ['dataSet', columnIndex],
                ld: column
            });
        }
    }

    function deletePost({ columnIndex, postIndex }: { columnIndex: number, postIndex: number }) {
        let oldColumn = doc.value.data.dataSet[columnIndex];
        if (oldColumn) {
            let newColumn: column = JSON.parse(JSON.stringify(oldColumn));
            newColumn.posts.splice(postIndex, 1);
            doc.value.submitOp({
                p: ['dataSet', columnIndex],
                ld: oldColumn,
                li: newColumn
            });
        }
    }

    function deleteAllPosts({ columnIndex }: { columnIndex: number }) {
        let oldColumn = doc.value.data.dataSet[columnIndex];
        if (oldColumn) {
            let newColumn: column = JSON.parse(JSON.stringify(oldColumn));
            newColumn.posts = [];
            doc.value.submitOp({
                p: ['dataSet', columnIndex],
                ld: oldColumn,
                li: newColumn
            });
        }
    }

    function deleteComment({ columnIndex, postIndex, commentIndex }: { columnIndex: number, postIndex: number, commentIndex: number }) {
        metaStore.deleteComment({index:columnIndex, postIndex, commentIndex});
    }

    return {
        doc,
        dataSet,
        isHeadlineEdit,
        handleMode,
        isCanvasOpen,
        file,
        showLoadingSpinner,
        elementTitle,
        elementText,
        elementBody,
        elementInfo,
        postIndex,
        columnIndex,
        movePostIndex,
        moveColumnIndex,
        showComments,
        showAdministrationSidebar,
        openColumnId,
        openPostId,
        insertAtIndex,
        commentIndex,
        elementColor,
        getDeletable,
        getMovable,
        getCommentable,
        getHeaderEditable,
        getFullUsable,
        getOneColumn,
        getCount,
        getLockState,
        getDataSet,
        getStyle,
        getLikesAllowed,
        getCommentsAllowed,
        getEmbeddedsAllowed,
        getColumnAddPosition,
        getHeadline,
        getSubHeadline,
        getHashcode,
        getNoPassHash,
        getAdminMail,
        getDashboardHash,
        getUserType,
        openMenu,
        createColumn,
        createPost,
        createComment,
        editColumnProps,
        editColumnPosition,
        editPostProps,
        editPostPosition,
        editCommentProps,
        editCommentPosition,
        likePost,
        likeComment,
        editHeadlines,
        editStyle,
        editRight,
        deleteColumn,
        deletePost,
        deleteAllPosts,
        deleteComment
    };
});
