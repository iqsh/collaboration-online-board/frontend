// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import { adminMenuEnum, handleModeEnum, paneTypes, userType as userTypes } from '@/enums/enums';
import { arrayMove, generateUUID } from "@/lib/utils";
import type { column, post } from "@/types/schedule";
import { defineStore } from 'pinia';
import { computed, ref } from 'vue';
import type { element, comment, globalStyle } from "@/types/shared";


export const useMetaStore = defineStore('meta', () => {
    const userType = ref<userTypes>(userTypes.USER);
    const paneType = ref<paneTypes>(paneTypes.NONE);
    const hashcode = ref<string>('');
    const adminMail = ref<string>('');
    const noPassHash = ref<string>('');
    const dashboard = ref<string>('');

    //administraton menu values
    const openMenu = ref<adminMenuEnum>(adminMenuEnum.NONE);

    // shared pane data
    const doc = ref<any>(null);
    const dataSet = ref<any[]|null>([]);
    const isHeadlineEdit = ref<boolean>(false);
    const handleMode = ref<handleModeEnum>(handleModeEnum.NONE);
    const isCanvasOpen = ref<boolean>(false);
    const file = ref<any>(null);
    const showLoadingSpinner = ref<boolean>(false);
    const elementTitle = ref<string>('');
    const elementText = ref<string>('');
    const elementBody = ref<any>('');
    const elementInfo = ref<string>('');
    const postIndex = ref<number>(-1);
    const showComments = ref<boolean>(false);
    const showAdministrationSidebar = ref<boolean>(false);
    const commentIndex = ref<number>(-1);
    const elementColor = ref<string>('');

    const getDeletable = computed(() => userType.value !== userTypes.USER ? true : doc.value && doc.value.data ? doc.value.data.unlocked.all && doc.value.data.unlocked.delete : false);
    const getMovable = computed(() => userType.value !== userTypes.USER ? true : doc.value && doc.value.data ? doc.value.data.unlocked.all && doc.value.data.unlocked.move : false);
    const getCommentable = computed(() => userType.value !== userTypes.USER ? true : doc.value && doc.value.data ? doc.value.data.unlocked.commentable : false);
    const getFullUsable = computed(() => userType.value!== userTypes.USER ? true : doc.value && doc.value.data ? doc.value.data.unlocked.all : false);
    const getLockState = computed(() => doc.value && doc.value.data ? doc.value.data.unlocked : null);
    const getDataSet = computed(() => dataSet.value);
    const getCount = computed(() => doc.value && doc.value.data ? doc.value.data.count : 0);
    const getStyle = computed(() => doc.value && doc.value.data ? doc.value.data.style : null);
    const getLikesAllowed = computed(() => doc.value && doc.value.data ? doc.value.data.style.likesAllowed : false);
    const getCommentsAllowed = computed(() => doc.value && doc.value.data ? doc.value.data.style.commentsAllowed : false);
    const getEmbeddedsAllowed = computed(() => doc.value && doc.value.data ? doc.value.data.style.embeddedsAllowed : false);
    const getHeadline = computed(() => doc.value && doc.value.data ? doc.value.data.headline : '');
    const getSubHeadline = computed(() => doc.value && doc.value.data ? doc.value.data.subheadline : '');
    const getHashcode = computed(() => hashcode.value);
    const getNoPassHash = computed(() => noPassHash.value);
    const getAdminMail = computed(() => adminMail.value);
    const getDashboardHash = computed(() => dashboard.value);
    const getUserType = computed(() => userType.value);

    const _getColumnOrPost = ({index, id}:{index?:number, id?: string})=>{
        let elementIndex = index !== undefined ? index : 0;
        let oldValue = null;
        if (id) {
            oldValue = doc.value.data.dataSet.find((x: any) => id == x.id) as any;
            elementIndex = doc.value.data.dataSet.indexOf(oldValue);
        }
        else if (index !== undefined) {
            oldValue = doc.value.data.dataSet[index] as any;
        }
        if (oldValue) {
            let newValue: any = JSON.parse(JSON.stringify(oldValue));
            return { newValue , oldValue, elementIndex } as { newValue:any, oldValue:any, elementIndex:number };
        }else{
            return null;
        }
    };

    const createPost = ({ columnId, columnIndex, text, title, color, shape, info, body, index, left, top }:
        { columnId?: string, columnIndex?: number, text: string, title?: string, color?: string, shape?: string, info?: string, body?: string, index?: number, left?: number, top?: number }) => {
        let id = generateUUID();
        let post: element = {
            id: id,
            title: { post: text, headline: title ? title : "" },
            color: color ? color : getStyle.value.commentColor,
            shape: shape ? shape : getStyle.value.shape,
            commentLength: 0,
            comments: [],
            visible: true,
            authorized: getUserType.value !== userTypes.USER ? true : !getStyle.value.authorize
        };
        if (info)
            post.info = info;
        if (body)
            post.body = body;
        if(paneType.value === paneTypes.FREEFORM)
            post.styleObject =  { left: String(left ? left : 0 + "px"), top: String(top ? top : "0" + "px") };
        if(paneType.value === paneTypes.SCHEDULE || paneType.value === paneTypes.STREAM || paneType.value === paneTypes.TIMELINE)
            post.likes = 0;
        if(paneType.value === paneTypes.FREEFORM || paneType.value === paneTypes.TIMELINE){
            const position = paneType.value === paneTypes.FREEFORM?getCount.value:index !== undefined ? index : getDataSet.value?.length ?? 0;
            doc.value.submitOp([{
                p: ['dataSet', position],
                li: post
            }, {
                p: ['count'],
                na: 1
            }]);
            return id;
        }
        if(paneType.value === paneTypes.SCHEDULE || paneType.value === paneTypes.STREAM){
            if(_getColumnOrPost({id: columnId, index: columnIndex})!==null){
                const { newValue: newColumn, oldValue: oldColumn, elementIndex } = _getColumnOrPost({ id: columnId, index: columnIndex }) ?? {} as { newValue: column, oldValue: column, elementIndex: number };
                if (index) {
                    newColumn.posts.splice(index, 0, post as post);
                } else {
                    if (getStyle.value.columnAddPosition === "bottom") {
                        newColumn.posts.push(post as post);
                    } else {
                        newColumn.posts.unshift(post as post);
                    }
                }
                newColumn.postLength++;
                doc.value.submitOp({
                    p: ['dataSet', elementIndex],
                    ld: oldColumn,
                    li: newColumn
                });
                return id;
            };
        }
    };

    const createComment = ({ index, body, text, type, shape, color, info, postIndex }: { index: number, body?: any, text: string, type?: string, shape?: string, color?: string, info?: string, postIndex?:number }) => {
        let oldDataSet: element = doc.value.data.dataSet[index];
        if (oldDataSet) {
            let newDataSet: element | column = JSON.parse(JSON.stringify(oldDataSet));
            let id = generateUUID();
            let comment: comment = {
                id: id, text: text, likes: 0,
                shape: shape ? shape : getStyle.value.shape, color: color ? color : getStyle.value.commentColor,
                visible: true
            };
            if (body) {
                comment.body = body;
            }
            if (info) {
                comment.info = info;
            }
            if(paneType.value === paneTypes.FREEFORM || paneType.value === paneTypes.TIMELINE && 'comments' in newDataSet){
                (newDataSet as element).comments.push(comment);
                (newDataSet as element).commentLength++;
            }
            else if((paneType.value === paneTypes.SCHEDULE || paneType.value === paneTypes.STREAM) && 'posts' in newDataSet && postIndex!==undefined){
                newDataSet.posts[postIndex].comments.push(comment);
                newDataSet.posts[postIndex].commentLength++;
            }
            doc.value.submitOp([{
                p: ['dataSet', index],
                ld: oldDataSet,
                li: newDataSet
            }]);
        }
    };


    const editPostProps = ({ index, id, text, title, info, body, color, shape, height, width, visible, authorized, postIndex }: { index?: number, id?: string, text?: string, title?: string, info?: string, body?: any, color?: string, shape?: string, height?: number, width?: number, visible?: boolean, authorized?: boolean, postIndex?:number }) => {
        if(_getColumnOrPost({id:id, index:index})!==null){
            const { newValue, oldValue, elementIndex } = _getColumnOrPost({id:id, index:index}) ?? {} as { newValue: column | element, oldValue: column | element, elementIndex: number };
            const post: element = 'posts' in newValue && postIndex!==undefined ?newValue.posts[postIndex]:newValue;
            if ((text !== undefined && ((text.length === 0 && title !== undefined && title.length > 0) || text.length > 0)))
                post.title.post = text;
            if (title !== undefined)
                post.title.headline = title;
            if (body !== undefined)
                post.body = body;
            if (info)
                post.info = info;
            if (color)
                post.color = color;
            if (shape)
                post.shape = shape;
            if (width && height)
                post.size = { width: width, height: height };
            if (visible !== undefined)
                post.visible = visible;
            if (authorized !== undefined)
                post.authorized = authorized;
            doc.value.submitOp([{
                p: ['dataSet', elementIndex],
                ld: oldValue,
                li: newValue
            }]);
            return post.id;
        }
    };

    const likePost = ({ index, id, postIndex }: { index?: number, id?: string, postIndex?:number }) => {
        if(_getColumnOrPost({id:id, index:index})!==null){
            const { newValue, oldValue, elementIndex } = _getColumnOrPost({id:id, index:index}) ?? {} as { newValue: column | element, oldValue: column | element, elementIndex: number };
            const post: element = 'posts' in newValue && postIndex!==undefined ?newValue.posts[postIndex]:newValue;
            post.likes!++;
            doc.value.submitOp({
                p: ['dataSet', elementIndex],
                ld: oldValue,
                li: newValue
            });
        }
    };

    const editCommentProps = ({ index, id, postIndex, commentIndex, text, info, body, type, color, shape, visible }: { index?: number, id?: string, postIndex?:number, commentIndex: number, text?: string, info?: string, body?: string, type?: string, color?: string, shape?: string, visible?: boolean }) => {
        if(_getColumnOrPost({id:id, index:index})!==null){
            const { newValue, oldValue, elementIndex } = _getColumnOrPost({id:id, index:index}) ?? {} as { newValue: column | element, oldValue: column | element, elementIndex: number };
            const post: element = 'posts' in newValue && postIndex!==undefined ?newValue.posts[postIndex]:newValue;
            if (text)
                post.comments[commentIndex].text = text;
            if (body !== undefined)
                post.comments[commentIndex].body = body;
            if (info)
                post.comments[commentIndex].info = info;
            if (type)
                post.comments[commentIndex].type = type;
            if (color)
                post.comments[commentIndex].color = color;
            if (shape)
                post.comments[commentIndex].shape = shape;
            if (visible !== undefined)
                post.comments[commentIndex].visible = visible;
            doc.value.submitOp({
                p: ['dataSet', elementIndex],
                ld: oldValue,
                li: newValue
            });
        }
    };

    const editCommentPosition = ({ index, id, postIndex, commentIndex, direction }: { index?: number, id?: string, postIndex?:number, commentIndex: number, direction: string }) => {
        if(_getColumnOrPost({id:id, index:index})!==null){
            const { newValue, oldValue, elementIndex } = _getColumnOrPost({id:id, index:index}) ?? {} as { newValue: column | element, oldValue: column | element, elementIndex: number };
            const post: element = 'posts' in newValue && postIndex!==undefined ?newValue.posts[postIndex]:newValue;
            let newCommentIndex = commentIndex;
            let commentArray = post.comments;
            const arrayLength = commentArray.length;
            if (commentIndex > 0 && (commentIndex < arrayLength - 1))
                newCommentIndex = direction === "+1" ? commentIndex + 1 : commentIndex - 1;
            else if (commentIndex === 0)
                newCommentIndex = direction === "+1" ? commentIndex + 1 : commentIndex;
            else if (commentIndex === arrayLength - 1)
                newCommentIndex = direction === "+1" ? commentIndex : commentIndex - 1;
            commentArray = arrayMove(commentArray, commentIndex, newCommentIndex);
            post.comments = commentArray;
            doc.value.submitOp({
                p: ['dataSet', elementIndex],
                ld: oldValue,
                li: newValue
            });
        }
    };

    const likeComment = ({ index, id, postIndex, commentIndex }: { index?: number, id?: string, postIndex?:number, commentIndex: number }) => {
        if(_getColumnOrPost({id:id, index:index})!==null){
            const { newValue, oldValue, elementIndex } = _getColumnOrPost({id:id, index:index}) ?? {} as { newValue: column | element, oldValue: column | element, elementIndex: number };
            const comment: comment = ('posts' in newValue && postIndex!==undefined ?newValue.posts[postIndex]:newValue).comments[commentIndex];
            comment.likes++;
            doc.value.submitOp({
                p: ['dataSet', elementIndex],
                ld: oldValue,
                li: newValue
            });
        }
    };

    const editHeadlines = ({ headline, subheadline }: { headline: string, subheadline: string }) => {
        let oldHeadline = doc.value.data.headline;
        let oldSubheadline = doc.value.data.subheadline;
        doc.value.submitOp([{
            p: ['headline'],
            od: oldHeadline,
            oi: headline
        }, {
            p: ['subheadline'],
            od: oldSubheadline,
            oi: subheadline
        }]);
    };

    const editStyle = (style: globalStyle) => {
        let oldStyle = doc.value.data.style;
        doc.value.submitOp([{
            p: ['style'],
            od: oldStyle,
            oi: style
        }]);
    };

    const editRight = ({ type, value }: { type: string, value: boolean }) => {
        let newState = JSON.parse(JSON.stringify(getLockState.value));
        newState[type] = value;
        doc.value.submitOp([{
            p: ['unlocked'],
            od: getLockState.value,
            oi: newState
        }]);
    };

    const deleteComment = ({ index, id, commentIndex, postIndex }: { index?: number, id?: string, commentIndex: number, postIndex?: number }) => {
        if(_getColumnOrPost({id:id, index:index})!==null){
            const { newValue, oldValue, elementIndex } = _getColumnOrPost({id:id, index:index}) ?? {} as { newValue: column | element, oldValue: column | element, elementIndex: number };
            const post: element = 'posts' in newValue && postIndex!==undefined ?newValue.posts[postIndex]:newValue;
            post.comments.splice(commentIndex, 1);
            doc.value.submitOp({
                p: ['dataSet', elementIndex],
                ld: oldValue,
                li: newValue
            });
        }
    };

    return {
        userType,
        paneType,
        hashcode,
        adminMail,
        noPassHash,
        dashboard,
        openMenu,
        doc,
        dataSet,
        isHeadlineEdit,
        handleMode,
        isCanvasOpen,
        file,
        showLoadingSpinner,
        elementTitle,
        elementText,
        elementBody,
        elementInfo,
        postIndex,
        showComments,
        showAdministrationSidebar,
        commentIndex,
        elementColor,

        getDeletable,
        getMovable,
        getCommentable,
        getFullUsable,
        getLockState,
        getDataSet,
        getCount,
        getStyle,
        getLikesAllowed,
        getCommentsAllowed,
        getEmbeddedsAllowed,
        getHeadline,
        getSubHeadline,
        getHashcode,
        getNoPassHash,
        getAdminMail,
        getDashboardHash,
        getUserType,

        createPost,
        createComment,
        editPostProps,
        likePost,
        editCommentProps,
        editCommentPosition,
        likeComment,
        editHeadlines,
        editStyle,
        editRight,
        deleteComment
    };
});
