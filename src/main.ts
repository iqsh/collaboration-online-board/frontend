// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import { createApp, defineAsyncComponent } from 'vue';
import { createPinia } from 'pinia';
import { createI18n } from 'vue-i18n';
import { createHead } from "@vueuse/head";


//@ts-ignore
import App from '@/App.vue';
import router from '@/router';
import 'remixicon/fonts/remixicon.css';
import 'highlight.js/styles/github.css';
import "@fontsource/comic-neue"; 

const i18n = createI18n({locale: 'de', globalInjection: true, warnHtmlInMessage: false, warnHtmlMessage: false});
const config = await fetch(import.meta.env.BASE_URL+"configuration.json").then((res) => res.json()).catch((e) => console.error("Cannot fetch configuration.json file"));

await fetch(import.meta.env.BASE_URL+"locales/de.json").then(response => response.json())
.then(msgs => {
    i18n.global.setLocaleMessage('de', msgs);
 });
const head = createHead();
const app = createApp(App)
app.config.globalProperties.$configuration = config;

app.provide('configuration', config);
app.component('Tooltip', defineAsyncComponent(() => import('@/components/general/Tooltip.vue')));
app.use(createPinia());
app.use(router);
app.use(i18n);
app.use(head);

app.mount('#app')

export {config, i18n};
