// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import { fileURLToPath, URL } from 'url';
import { defineConfig } from 'vite';
import vue from '@vitejs/plugin-vue';
import vueJsx from '@vitejs/plugin-vue-jsx';
import vueI18n from '@intlify/unplugin-vue-i18n/vite';
import topLevelAwait from "vite-plugin-top-level-await";

const isProd = process.env.NODE_ENV !== 'development';
// https://vitejs.dev/config/
export default defineConfig({
    plugins: [
      vue({
        template:{
          compilerOptions: {
            isCustomElement: (tag) => tag.startsWith('cropper-')
          }
        }
      }), 
      vueJsx(), 
      vueI18n({
        // if you want to use Vue I18n Legacy API, you need to set `compositionOnly: false`
        // compositionOnly: false,
        runtimeOnly: false
      }),
      topLevelAwait({
        // The export name of top-level await promise for each chunk module
        promiseExportName: "__tla",
        // The function to generate import names of top-level await promise in each chunk module
        promiseImportName: i => `__tla_${i}`
      }),
      /*splitVendorChunkPlugin(),
      {
        name: "static-js",
        apply: "serve",
        enforce: "pre",
        resolveId(source,importer){
          if(source.endsWith('frame.bundle.js')){
            return "\ufeff"+source;
          }
        }
      }*/
    ],
    server: {
      host: true,
      watch: {
        usePolling: true
      }

    },
    resolve: {
      alias: {
        '@': fileURLToPath(new URL('./src', import.meta.url)),
      }
    },
    optimizeDeps: {
      include: [
        'sharedb-client-browser/dist/sharedb-client-umd.cjs'
      ]
    },
    build:{
      minify: 'esbuild'
    },
    esbuild: isProd
    ? {
        drop: ['console', 'debugger'],
      }
      :{}
});