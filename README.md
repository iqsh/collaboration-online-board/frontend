### Deutsch
## Collaborative Online Board - Frontend
Verteilt die statischen Dateien (HTML, JS, CSS) an die Nutzenden. Die Software nutzt *Vue.js 3* als UI-Framework, *Pinia* als Vue Store und *ShareDB* für das *Realtime Database Backend* in Verbindung mit dem Coordinator. 

### System Anforderungen:
* NodeJS 16+ 
* NPM
* Libc6 (or libc6-compat)
* Ngnix oder ein anderer Webserver mit Unterstützung für Statische Dateien
* Getestet mit *Ubuntu 20.04 LTS* und *Alpine Linux*

### Installation von den Software-Abhängigkeiten:
Gehen Sie in den *frontend*-Ordner und führen Sie `npm install` aus.

### Erstellen und Verteilen mit dem Webserver:
Führen Sie `npm run build-dev` (ohne basePath) aus, um die Dateien von dem Frontend zu erstellen.  
Die erstellten Dateien sind anschließend unter */build* zu finden, welche mit dem Webserver verteilt werden müssen.

### Anpassungen:
Kopieren und benennen Sie die Dateien mit der Endung *.example* um, indem Sie *.example* von den jeweiligen Dateien entfernen. Ersetzen Sie die benötigten Bilder in */public* und passen Sie die Werte in */public/configuration.json* und */public/locales/de.json* an.  
Zum Setzen eines eigenen Titels muss in der *index.html* *FRONTEND_APP_NAME* gegen den gewünschten Titel ausgetauscht werden.

Für die Farbanpassungen müssen Sie die Farbwerte in */tailwind.config.js* für ein eigenes Branding vor dem Bauen ersetzen:
* #003064 - Primäre Farbe
* #00244a und #00244b - Primäre Farbe (Hover)
* #d60d4c - Akzent Farbe
* #be0c43 - Akzent Farbe (Hover)
* #a60a3b - Akzent Farne (Aktiv)
* #ffffff - Text Farbe (in beiden Fällen)

Für jede Personen-Kategorie für den KI-Chat müssen Sie die Farbwerte aus */public/configuration.json* in die */src/style.css* als eigene Klassen ergänzen (zum Beispiel auf Basis der Farbnamen von TailwindCSS).
````
/* beispielsweise mit Hilfe der Farbklasse von TailwindCSS angewendet */
.emerald-700 {
    @apply bg-emerald-700
}
````

*** 

### English
## Collaborative Online Board - Frontend
Serves the static HTML, JS, CSS files to the users. The software uses *Vue.js 3* as UI-Framework, *Pinia* as Vue Store and *ShareDB* for the *Realtime database backend* for the coordination with the coordinator.  

### System Requirements:
* NodeJS 16+ 
* NPM
* Libc6 (or libc6-compat)
* Ngnix or another webserver with static file serve support
* Tested with *Ubuntu 20.04 LTS* and *Alpine Linux*

### Installation of the software dependencies:
Go into the *frontend* folder and run `npm install`. 

### Build and serve with the webserver:
Run `npm run build-dev` (without basePath) to build the frontend files. 
Builded files are in */build*, which needs to be served with the webserver.

### Customization:
Copy and rename the files with *.example* ending (remove the *.example*). Replace necessary images in */public* and adapt the values in */public/configuration.json* and */public/locales/de.json*. 
To set an own title, change the title *FRONTEND_APP_NAME* in the *index.html* with your title. 
For color customization replace the color codes in */tailwind.config.js* before building for own branding:
* #003064 - Primary color
* #00244a and #00244b - Primary color (hover)
* #d60d4c - Accent color
* #be0c43 - Accent color (hover)
* #a60a3b - Accent color (active)
* #ffffff - Text color (in both cases)

For each person category for the AI chat, you must add the color values from */public/configuration.json* to */src/style.css* as separate classes (for example, based on the color names of TailwindCSS).
````
/* applied using the color class of TailwindCSS, for example */
.emerald-700 {
    @apply bg-emerald-700
}
````
  
Currently it is in the colors of the state *Schleswig-Holstein*.