// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later

/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./index.html",
    "./src/**/*.{vue,js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      colors: {
        accent1:{
          default: "#003064",
          hover: "#00254D",
          text: "#ffffff"
        },
        accent2:{
          default: "#D60D4C",
          hover: "#A90A3C",
          text: "#ffffff"
        },
      },
      borderRadius:{
        'xl': '1em',
        '2xl': '1.5em' 
      },
      keyframes: {
        wiggle: {
          '0%, 100%': { transform: 'rotate(-12deg)' },
          '50%': { transform: 'rotate(12deg)' },
        }
      },
      animation: {
        wiggle: 'wiggle 0.3s ease-in-out infinite',
      }
    },
  },
  plugins: [],
}

